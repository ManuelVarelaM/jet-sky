﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class OptionsManager : MonoBehaviour
{
    public bool active;

    private String[] languages = {"Hello", "Hola"};
    public Language languageSelected;

    public OptionsButton currentOption;
    public OptionsButton initOption;
    public RectTransform optionRect;

    public Image selector;
    public bool selected;

    public Image volumeMusicBar;
    public Image volumeSoundBar;
    public Image controlJetImage;
    public Image controlSkyImage;

    public ControlManager controlManager;
    public GameObject setControlJet;
    public GameObject setControlSky;

    public TextMeshProUGUI languageText;

    public GameObject arrowLeft;
    public GameObject arrowRight;

    public string jetControlImageName;
    public string skyControlImageName;

    private AudioSource audioSource;

    void Start()
    {
        active = false;
        selected = false;
        currentOption = initOption;
        languageSelected = Options.language;

        arrowLeft.SetActive(false);
        arrowRight.SetActive(false);

        optionRect = currentOption.GetComponent<RectTransform>();
        selector.transform.position = currentOption.transform.position;
        selector.rectTransform.sizeDelta = optionRect.sizeDelta;

        audioSource = this.GetComponent<AudioSource>();

        UpdateControlImages();
    }

    private void FixedUpdate()
    {
        // Selector
        optionRect = currentOption.GetComponent<RectTransform>();
        selector.transform.position = Vector3.Lerp(selector.transform.position, currentOption.transform.position, 0.2f);
        selector.rectTransform.sizeDelta = Vector3.Lerp(selector.rectTransform.sizeDelta, optionRect.sizeDelta, 0.2f);

        // Barras de volumen
        volumeMusicBar.fillAmount = Mathf.Lerp(volumeMusicBar.fillAmount, Options.musicVolume / 5f, 0.2f);
        volumeSoundBar.fillAmount = Mathf.Lerp(volumeSoundBar.fillAmount, Options.soundVolume / 5f, 0.2f);

        // Imagenes controles
        UpdateControlImages();
    }

    void Update()
    {  
        // Idioma
        languageText.text = languages[(int) languageSelected];

        RectTransform rect = this.GetComponentInParent<RectTransform>(); 

        if (active)
            rect.localPosition = Vector3.Lerp(rect.localPosition, new Vector3(0, 160, 0), 0.1f);

        else
            rect.localPosition = Vector3.Lerp(rect.localPosition, new Vector3(0, 480, 0), 0.1f);

        if (Options.optionsOpen)
        {
            // Boton seleccionado
            if (selected)
            {
                if (Control.accept)
                {
                    currentOption.action.Invoke();
                }

                if (currentOption.showArrows)
                {
                    arrowLeft.SetActive(true);
                    arrowRight.SetActive(true);
                }

                if (currentOption.name == "controlJet")
                {
                    controlJetImage.gameObject.SetActive(false);
                    setControlJet.SetActive(true);

                    if (controlManager.SetControlJet())
                    {
                        selected = false;
                        UpdateControlImages();

                        controlJetImage.gameObject.SetActive(true);
                        setControlJet.SetActive(false);
                    }
                }

                if (currentOption.name == "controlSky")
                {
                    controlSkyImage.gameObject.SetActive(false);
                    setControlSky.SetActive(true);

                    if (controlManager.SetControlSky())
                    {
                        selected = false;
                        UpdateControlImages();

                        controlSkyImage.gameObject.SetActive(true);
                        setControlSky.SetActive(false);
                    }
                }

                if (currentOption.name == "music")
                {
                    if (Options.musicVolume <= 0)
                        arrowLeft.SetActive(false);
                
                    else if (Options.musicVolume >= 5)
                        arrowRight.SetActive(false);
                }

                if (currentOption.name == "sound")
                {
                    if (Options.soundVolume <= 0)
                        arrowLeft.SetActive(false);

                    else if (Options.soundVolume >= 5)
                        arrowRight.SetActive(false);
                }

                Vector3 toPositionLeft = currentOption.transform.localPosition - new Vector3(76, 0, 0);
                Vector3 toPositionRight = currentOption.transform.localPosition + new Vector3(76, 0, 0);
                arrowLeft.transform.localPosition = Vector3.Lerp(arrowLeft.transform.localPosition, toPositionLeft, 0.2f);
                arrowRight.transform.localPosition = Vector3.Lerp(arrowRight.transform.localPosition, toPositionRight, 0.2f);

                if (Control.left)
                {
                    arrowLeft.transform.localPosition = currentOption.transform.localPosition - new Vector3(84, 0, 0);
                    currentOption.actionLeft.Invoke();
                }

                if (Control.right)
                {
                    arrowRight.transform.localPosition = currentOption.transform.localPosition + new Vector3(84, 0, 0);
                    currentOption.actionRight.Invoke();
                }
            }

            else
            {
                if (Control.accept)
                {
                    if (currentOption.selectable)
                    {
                        selected = true;

                        arrowLeft.transform.localPosition = currentOption.transform.localPosition - new Vector3(64, 0, 0);
                        arrowRight.transform.localPosition = currentOption.transform.localPosition + new Vector3(64, 0, 0);

                        playSound("menu_select");
                    }

                    else
                    {
                        currentOption.action.Invoke();

                        playSound("menu_select");
                    }
                }

                arrowLeft.SetActive(false);
                arrowRight.SetActive(false);

                if (Control.left & currentOption.buttonLeft != null)
                {
                    currentOption = currentOption.buttonLeft;
                    playSound("Menu/Menu abajo");
                }

                if (Control.right & currentOption.buttonRight != null)
                {
                    currentOption = currentOption.buttonRight;
                    playSound("Menu/Menu abajo");
                }

                if (Control.up & currentOption.buttonUp != null)
                {
                    currentOption = currentOption.buttonUp;
                    playSound("Menu/Menu abajo");
                }

                if (Control.down & currentOption.buttonDown != null)
                {
                    currentOption = currentOption.buttonDown;
                    playSound("Menu/Menu abajo");
                }
            }
        }
    }

    public void ChangeMusicVolume(int i)
    {
        if (Options.musicVolume + i <= 5 & Options.musicVolume + i >= 0)
            Options.musicVolume += i;
    }

    public void ChangeSoundVolume(int i)
    {
        if (Options.soundVolume + i <= 5 & Options.soundVolume + i >= 0)
            Options.soundVolume += i;
    }

    public void ChangeLanguage(int i)
    {
        languageSelected += i;

        if (languageSelected < 0)
            languageSelected = (Language) Enum.GetValues(typeof(Language)).Length - 1;

        if ((int)languageSelected >= Enum.GetValues(typeof(Language)).Length)
            languageSelected = 0;
    }

    public void SetLanguage()
    {
        Options.language = languageSelected;
    }

    public void Unselect()
    {
        selected = false;
    }

    private void UpdateControlImages()
    {
        // Control
        jetControlImageName = "";
        skyControlImageName = "";
        int jetImageFrame = 0;
        int skyImageFrame = 0;

        if (Control.jet != null)
        {
            jetControlImageName += "Xbox";
            if (Control.sharingControl)
                jetImageFrame = 1;
        }

        else
        {
            jetControlImageName += "Keys";
            jetImageFrame = 0;
        }

        if (Control.sky != null)
        {
            skyControlImageName += "Xbox";
            if (Control.sharingControl)
                skyImageFrame = 2;
        }

        else
        {
            skyControlImageName += "Mouse";
            skyImageFrame = 0;
        }
        
        Sprite jetControlSprite = Resources.LoadAll<Sprite>("2D Assets/controls/control" + jetControlImageName)[jetImageFrame];
        controlJetImage.GetComponent<Image>().sprite = jetControlSprite;

        Sprite skyControlSprite = Resources.LoadAll<Sprite>("2D Assets/controls/control" + skyControlImageName)[skyImageFrame];
        controlSkyImage.GetComponent<Image>().sprite = skyControlSprite;
    }

    public void Activate()
    {
        Options.optionsOpen = true;
        this.active = true;
    }

    public void Exit()
    {
        Options.optionsOpen = false;
        this.active = false;
    }

    private void playSound(string source)
    {
        audioSource.clip = Resources.Load<AudioClip>("Audio/SFX/" + source);
        audioSource.Play();
    }
}
