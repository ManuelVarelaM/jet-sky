﻿using InControl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Language {English, Spanish};

public static class Options
{
    public static Language language = 0;

    public static int musicVolume = 5; // maximo 5
    public static int soundVolume = 5; // maximo 5

    // Indica si el menu de opciones está abierto
    public static bool optionsOpen = false;
}
