﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OptionsButton : MonoBehaviour
{
    public string name;
    public bool selectable;
    public bool showArrows;

    public UnityEvent action;
    public UnityEvent actionLeft;
    public UnityEvent actionRight;

    public OptionsButton buttonLeft;
    public OptionsButton buttonRight;
    public OptionsButton buttonUp;
    public OptionsButton buttonDown;
}
