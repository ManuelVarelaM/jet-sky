﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider))]
public class Bounce : MonoBehaviour
{
    public float bounceForce = 1;

    private void OnCollisionEnter(Collision collision)
    {
        collision.collider.attachedRigidbody.AddForceAtPosition(Vector3.one * bounceForce, collision.GetContact(0).point);
       // collision.collider.attachedRigidbody.AddExplosionForce(collision.collider.attachedRigidbody.velocity.magnitude * bounceForce, collision.GetContact(0).point,2);
    }
}
