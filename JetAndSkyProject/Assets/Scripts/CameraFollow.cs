﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public FloatValue levelWidth;

    public Transform player;
    public Transform aim;
    public Transform target;
    public bool scrollToPlayer;

    public float speed;
    public float distance;
    public float maxDistance;

    public Vector3 scrollTo;

    private void FixedUpdate()
    {
        Vector3 currentPosition = transform.position;

        if (scrollToPlayer)
        {
            distance = maxDistance + player.position.z;
            scrollTo = Vector3.Lerp(player.position, aim.position, 0.2f) - new Vector3(0, JetSky.cameraDistance, distance);
        }

        else
        {
            scrollTo = target.transform.position + new Vector3(0, 0, distance);
        }

        if (scrollTo.x < -levelWidth.value)
            scrollTo = new Vector3(-levelWidth.value, scrollTo.y, scrollTo.z);

        if (scrollTo.x > levelWidth.value)
            scrollTo = new Vector3(levelWidth.value, scrollTo.y, scrollTo.z);

        transform.position = Vector3.Lerp(currentPosition, scrollTo, 0.1f);
    }
}
