﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeaPadding : MonoBehaviour
{
    public float localSpeed;
    public FloatValue timeSpeed;
    public FloatValue levelSpeed;

    private float offset;
    public Renderer rend;

    void OnEnable()
    {
        rend = GetComponent<Renderer>();
    }

    void Update()
    {
        offset += JetSky.movingSpeed * timeSpeed.value * Time.deltaTime * localSpeed;
        rend.material.SetTextureOffset("_MainTex", new Vector2(0, -offset));
    }
}
