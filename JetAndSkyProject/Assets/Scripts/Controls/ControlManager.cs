﻿using InControl;
using UnityEngine;

public class ControlManager : MonoBehaviour
{
    private bool jetUpFlag;
    private bool jetLeftFlag;
    private bool jetRightFlag;
    private bool jetDownFlag;

    private bool skyUpFlag;
    private bool skyLeftFlag;
    private bool skyRightFlag;
    private bool skyDownFlag;
    
    void Update()
    {
        // Como los input de interfaz son generales para Jet y Sky, los valores de cada uno son locales
        bool jetStart   = false, jetAccept  = false, jetCancel  = false;
        bool jetUp      = false, jetDown    = false, jetLeft    = false, jetRight = false;

        bool skyStart   = false, skyAccept  = false, skyCancel  = false;
        bool skyUp      = false, skyDown    = false, skyLeft    = false, skyRight = false;

        // Ambos están usando el mismo gamepad
        if (Control.jet == Control.sky & Control.jet != null)
        {
            Control.sharingControl = true;

            // Axis
            Control.jetAxisX = Control.jet.LeftStickX;
            Control.jetAxisY = Control.jet.LeftStickY;
            Control.skyAxisX = Control.sky.RightStickX;
            Control.skyAxisY = Control.sky.RightStickY;

            Control.jetA = Control.jet.LeftTrigger.IsPressed;
            Control.jetB = Control.jet.LeftBumper;

            Control.skyA = Control.sky.RightTrigger.IsPressed;
            Control.skyB = Control.sky.RightBumper;

            jetAccept = Control.jet.Action1.WasPressed;
            jetCancel = Control.jet.Action2.WasPressed;
            jetStart  = Control.jet.CommandWasPressed;

            //Down
            if (!jetDownFlag & (Control.jet.DPadDown || Control.jet.LeftStickDown))
            {
                jetDown = true;
                jetDownFlag = true;
            }

            else
            {
                jetDown = false;

                if (!(Control.jet.DPadDown || Control.jet.LeftStickDown))
                    jetDownFlag = false;
            }

            // Up
            if (!jetUpFlag & (Control.jet.DPadUp || Control.jet.LeftStickUp))
            {
                jetUp = true;
                jetUpFlag = true;
            }

            else
            {
                jetUp = false;

                if (!(Control.jet.DPadUp || Control.jet.LeftStickUp))
                    jetUpFlag = false;
            }

            // Right
            if (!jetRightFlag & (Control.jet.DPadRight || Control.jet.LeftStickRight))
            {
                jetRight = true;
                jetRightFlag = true;
            }

            else
            {
                jetRight = false;

                if (!(Control.jet.DPadRight || Control.jet.LeftStickRight))
                    jetRightFlag = false;
            }

            // Left
            if (!jetLeftFlag & (Control.jet.DPadLeft || Control.jet.LeftStickLeft))
            {
                jetLeft = true;
                jetLeftFlag = true;
            }

            else
            {
                jetLeft = false;

                if (!(Control.jet.DPadLeft || Control.jet.LeftStickLeft))
                    jetLeftFlag = false;
            }

            // Up
            if (!skyUpFlag & (Control.sky.RightStickUp))
            {
                skyUp = true;
                skyUpFlag = true;
            }

            else
            {
                skyUp = false;

                if (!(Control.sky.RightStickUp))
                    skyUpFlag = false;
            }

            // Right
            if (!skyRightFlag & (Control.sky.RightStickRight))
            {
                skyRight = true;
                skyRightFlag = true;
            }

            else
            {
                skyRight = false;

                if (!(Control.sky.RightStickRight))
                    skyRightFlag = false;
            }

            // Left
            if (!skyLeftFlag & (Control.sky.RightStickLeft))
            {
                skyLeft = true;
                skyLeftFlag = true;
            }

            else
            {
                skyLeft = false;

                if (!(Control.sky.RightStickLeft))
                    skyLeftFlag = false;
            }
        }

        else
        {
            Control.sharingControl = false;

            // Jet está usando el teclado
            if (Control.jet == null)
            {
                // Axis X
                if ((Input.GetKey(KeyCode.LeftArrow)        || Input.GetKey(KeyCode.A)) && 
                   !(Input.GetKey(KeyCode.RightArrow)       || Input.GetKey(KeyCode.D)))
                    Control.jetAxisX = -1;

                else if ((Input.GetKey(KeyCode.RightArrow)  || Input.GetKey(KeyCode.D)) && 
                        !(Input.GetKey(KeyCode.LeftArrow)   || Input.GetKey(KeyCode.A)))
                    Control.jetAxisX = 1;

                else
                    Control.jetAxisX = 0;

                // Axis Y
                if ((Input.GetKey(KeyCode.DownArrow)        || Input.GetKey(KeyCode.S)) && 
                   !(Input.GetKey(KeyCode.UpArrow)          || Input.GetKey(KeyCode.W)))
                    Control.jetAxisY = -1;

                else if ((Input.GetKey(KeyCode.UpArrow)     || Input.GetKey(KeyCode.W)) && 
                         !Input.GetKey(KeyCode.DownArrow)   || Input.GetKey(KeyCode.S))
                    Control.jetAxisY = 1;

                else
                    Control.jetAxisY = 0;

                Control.jetA        = Input.GetKey(KeyCode.Space);
                Control.jetB        = Input.GetKey(KeyCode.Escape);
                Control.jetOnA      = Input.GetKeyDown(KeyCode.Space);
                Control.jetOnB      = Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKeyDown(KeyCode.RightShift);

                jetStart    = Input.GetKeyDown(KeyCode.Return);
                jetAccept   = Input.GetKeyDown(KeyCode.Space);
                jetCancel   = Input.GetKeyDown(KeyCode.Escape);
                jetDown     = Input.GetKeyDown(KeyCode.DownArrow);
                jetUp       = Input.GetKeyDown(KeyCode.UpArrow);
                jetLeft     = Input.GetKeyDown(KeyCode.LeftArrow);
                jetRight    = Input.GetKeyDown(KeyCode.RightArrow);
            }

            // Jet está usando un gamepad
            else
            {
                Control.jetAxisX = Control.jet.LeftStickX;
                Control.jetAxisY = Control.jet.LeftStickY;

                Control.jetA = Control.jet.Action1.IsPressed;
                Control.jetB = Control.jet.Action2.IsPressed;
                Control.jetOnA = Control.jet.Action1.WasPressed;
                Control.jetOnB = Control.jet.Action2.WasPressed;
                
                jetAccept = Control.jet.Action1.WasPressed;
                jetCancel = Control.jet.Action2.WasPressed;
                jetStart = Control.jet.CommandWasPressed;

                //Down
                if (!jetDownFlag & (Control.jet.DPadDown || Control.jet.LeftStickDown))
                {
                    jetDown = true;
                    jetDownFlag = true;
                }

                else
                {
                    jetDown = false;

                    if (!(Control.jet.DPadDown || Control.jet.LeftStickDown))
                        jetDownFlag = false;
                }
                
                // Up
                if (!jetUpFlag & (Control.jet.DPadUp || Control.jet.LeftStickUp))
                {
                    jetUp = true;
                    jetUpFlag = true;
                }

                else
                {
                    jetUp = false;

                    if (!(Control.jet.DPadUp || Control.jet.LeftStickUp))
                        jetUpFlag = false;
                }

                // Right
                if (!jetRightFlag & (Control.jet.DPadRight || Control.jet.LeftStickRight))
                {
                    jetRight = true;
                    jetRightFlag = true;
                }

                else
                {
                    jetRight = false;

                    if (!(Control.jet.DPadRight || Control.jet.LeftStickRight))
                        jetRightFlag = false;
                }

                // Left
                if (!jetLeftFlag & (Control.jet.DPadLeft || Control.jet.LeftStickLeft))
                {
                    jetLeft = true;
                    jetLeftFlag = true;
                }

                else
                {
                    jetLeft = false;

                    if (!(Control.jet.DPadLeft || Control.jet.LeftStickLeft))
                        jetLeftFlag = false;
                }
            }

            // Sky está usando el mouse
            if (Control.sky == null)
            {
                Control.skyA = Input.GetMouseButton(0);
                Control.skyB = Input.GetMouseButton(1);
                Control.skyOnA = Input.GetMouseButtonDown(0);
                Control.skyOnB = Input.GetMouseButtonDown(1);

                Control.cursorX = GetWorldPositionOnPlane(Input.mousePosition, 0).x;
                Control.cursorY = GetWorldPositionOnPlane(Input.mousePosition, 0).y;
            }

            // Sky está usando un gamepad
            else
            {
                Control.skyAxisX = Control.sky.LeftStickX;
                Control.skyAxisY = Control.sky.LeftStickY;

                Control.skyA = Control.sky.Action1.IsPressed;
                Control.skyB = Control.sky.Action2.IsPressed;
                Control.skyOnA = Control.sky.Action1.WasPressed;
                Control.skyOnB = Control.sky.Action2.WasPressed;

                skyAccept = Control.sky.Action1.WasPressed;
                skyCancel = Control.sky.Action2.WasPressed;
                skyStart = Control.sky.CommandWasPressed;

                //Down
                if (!skyDownFlag & (Control.sky.DPadDown || Control.sky.LeftStickDown))
                {
                    skyDown = true;
                    skyDownFlag = true;
                }

                else
                {
                    skyDown = false;

                    if (!(Control.sky.DPadDown || Control.sky.LeftStickDown))
                        skyDownFlag = false;
                }

                // Up
                if (!skyUpFlag & (Control.sky.DPadUp || Control.sky.LeftStickUp))
                {
                    skyUp = true;
                    skyUpFlag = true;
                }

                else
                {
                    skyUp = false;

                    if (!(Control.sky.DPadUp || Control.sky.LeftStickUp))
                        skyUpFlag = false;
                }

                // Right
                if (!skyRightFlag & (Control.sky.DPadRight || Control.sky.LeftStickRight))
                {
                    skyRight = true;
                    skyRightFlag = true;
                }

                else
                {
                    skyRight = false;

                    if (!(Control.sky.DPadRight || Control.sky.LeftStickRight))
                        skyRightFlag = false;
                }

                // Left
                if (!skyLeftFlag & (Control.sky.DPadLeft || Control.sky.LeftStickLeft))
                {
                    skyLeft = true;
                    skyLeftFlag = true;
                }

                else
                {
                    skyLeft = false;

                    if (!(Control.sky.DPadLeft || Control.sky.LeftStickLeft))
                        skyLeftFlag = false;
                }
            }
        }

        Control.start   = jetStart  || skyStart;
        Control.up      = jetUp     || skyUp;
        Control.down    = jetDown   || skyDown;
        Control.left    = jetLeft   || skyLeft;
        Control.right   = jetRight  || skyRight;
        Control.accept  = jetAccept || skyAccept;
        Control.cancel  = jetCancel || skyCancel;
    }

    public bool SetControlJet()
    {
        InputDevice inputDevice = InputManager.ActiveDevice;

        if (JoinButtonWasPressedOnDevice(inputDevice))
        {
            Control.jet = inputDevice;

            if (Control.jet == Control.sky)
                Control.sharingControl = true;

            else
                Control.sharingControl = false;

            return true;
        }

        else if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Return))
        {
            Control.jet = null;
            return true;
        }

        return false;
    }

    public bool SetControlSky()
    {
        InputDevice inputDevice = InputManager.ActiveDevice;

        if (JoinButtonWasPressedOnDevice(inputDevice))
        {
            Control.sky = inputDevice;

            if (Control.jet == Control.sky)
                Control.sharingControl = true;

            else
                Control.sharingControl = false;

            return true;
        }

        else if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1))
        {
            Control.sky = null;
            return true;
        }

        return false;
    }

    public bool JoinButtonWasPressedOnDevice(InputDevice inputDevice)
    {
        return inputDevice.Action1.WasPressed || inputDevice.Action2.WasPressed || inputDevice.Action3.WasPressed || inputDevice.Action4.WasPressed;
    }

    public Vector3 GetWorldPositionOnPlane(Vector3 screenPosition, float z)
    {
        Ray ray = Camera.main.ScreenPointToRay(screenPosition);
        Plane xy = new Plane(Vector3.forward, new Vector3(0, 0, z));
        float distance;
        xy.Raycast(ray, out distance);
        return ray.GetPoint(distance);
    }
}
