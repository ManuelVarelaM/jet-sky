﻿using InControl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Control
{
    public static InputDevice jet;
    public static InputDevice sky;
    public static bool sharingControl;
    
    public static bool start;
    public static bool up;
    public static bool down;
    public static bool left;
    public static bool right;
    public static bool accept;
    public static bool cancel;

    public static float jetAxisX;
    public static float jetAxisY;
    public static float skyAxisX;
    public static float skyAxisY;
    public static float cursorX;
    public static float cursorY;

    public static bool jetA;
    public static bool jetB;
    public static bool jetOnA;
    public static bool jetOnB;

    public static bool skyA;
    public static bool skyB;
    public static bool skyOnA;
    public static bool skyOnB;
}
