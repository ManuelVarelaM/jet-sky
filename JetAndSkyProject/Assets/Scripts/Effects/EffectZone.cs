﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class EffectZone : MonoBehaviour
{
    private void OnCollisionStay(Collision collision)
    {
        if (collision.transform.GetComponent<EffectBehaviour>())
        {
            foreach (EffectBehaviour effect in collision.transform.GetComponents<EffectBehaviour>())
            {
                effect.Show(collision);
            }
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.transform.GetComponent<EffectBehaviour>())
        {
            foreach (EffectBehaviour effect in collision.transform.GetComponents<EffectBehaviour>())
            {
                effect.Hide();
            }
        }
    }
}
