﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransitionLine : MonoBehaviour
{
    void Update()
    {
        this.transform.localPosition -= new Vector3(0, 0.03f, 0);

        if (this.transform.localPosition.y < -2.7f)
        {
            this.transform.localPosition = new Vector3(0, 2.5f, 0);
        }
    }
}
