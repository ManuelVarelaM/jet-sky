﻿using UnityEngine;
using System.Collections;

public abstract class EffectBehaviour : MonoBehaviour
{
    public abstract void Show(Collision collision);
    public abstract void Hide();
}
