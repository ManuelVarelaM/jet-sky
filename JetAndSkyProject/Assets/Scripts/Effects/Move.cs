﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    public float speedX;
    public float speedY;

    // Update is called once per frame
    void Update()
    {
        this.transform.position += new Vector3(speedX, speedY, 0);
    }
}
