﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sine : MonoBehaviour
{
    public float speed;
    public float accel;
    public float magnitude;
    public float startX;
    public float delay;

    void Start()
    {
        startX = this.transform.localPosition.x;
    }

    void Update()
    {
        float posX = startX + Mathf.Sin(Time.time * speed + delay) * magnitude;
        this.transform.localPosition = new Vector3(posX, this.transform.localPosition.y, this.transform.localPosition.z);
    }
}
