﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider), typeof(Rigidbody), typeof(JetSky))]
public class BounceEffect : EffectBehaviour
{
    public override void Show(Collision coll)
    {
        GetComponent<Rigidbody>().AddForce(coll.contacts[0].normal * Random.Range(28,48) * -1, ForceMode.Force);
    }

    public override void Hide()
    {
        GetComponent<Rigidbody>().velocity = Vector3.zero;
    }
}
