﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class VehicleSparkEffect : EffectBehaviour
{
   [SerializeField] private ParticleSystem _spark;

    public override void Show(Collision coll)
    {
        _spark.transform.position = coll.GetContact(0).point;
        _spark.Play();
    }

    public override void Hide()
    {
        _spark.Stop();
    }
}
