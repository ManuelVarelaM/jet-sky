﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider), typeof(Rigidbody))]
public class Bullet : Entity
{
    private float damage;
    private float speed;

    public ParticleSystem impact_effect;
    public GameObject impact_effect_prefab;
    public SpriteRenderer _renderer;
    private float angle;

    Collider bulletCollider;

    public void Init(float damage, float speed, GameObject impact_effect_prefab)
    {
        this.damage = damage;
        this.speed = speed;
        this.bulletCollider = GetComponent<Collider>();
        this._renderer = GetComponent<SpriteRenderer>();
        this.impact_effect_prefab = impact_effect_prefab;

        gameObject.SetActive(false);
    }

    public void Shoot(Vector3 initPos, float angle, Entity shooter)
    {
        _renderer.enabled = true;
        transform.position = initPos;
        transform.rotation = Quaternion.AngleAxis(angle, transform.up) * transform.rotation;

        transform.rotation = Quaternion.Euler(transform.rotation.x, 0, transform.rotation.z);

        this.angle = angle;

        Collider shooterOrigin = shooter.GetComponent<Collider>();

        this.gameObject.SetActive(true);

        if (shooterOrigin != null)
        {
            Physics.IgnoreCollision(bulletCollider, shooterOrigin);
        }
    }

    private void FixedUpdate()
    {
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.position += transform.right * speed * GameManager.timeSpeed - new Vector3(0, JetSky.speedY * GameManager.timeSpeed, 0);
    }

    private void OnCollisionEnter(Collision collision)
    {
        _renderer.enabled = false;

        if (impact_effect == null)
        {
            impact_effect = Instantiate(impact_effect_prefab).GetComponent<ParticleSystem>();
        }

        impact_effect.transform.SetParent(null);
        impact_effect.transform.position = collision.contacts[0].point;
        impact_effect.transform.SetParent(collision.transform);
        impact_effect.transform.localScale = Vector3.one;


        impact_effect.Play();

        try
        {
            collision.gameObject.GetComponent<IDamageable>().GetDamage(damage);
        }
        catch
        {

        }

        gameObject.SetActive(false);
    }
}
