﻿using UnityEngine;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class Reward : MonoBehaviour {

    public UnityEvent onRewardClaimed;

    public FloatValue targetValue;
    public FloatValue delta_scriptableValue;
    public float delta_value;
    public bool isConstant;

    public Vector3 spawnPos;
    public Transform parent;
    public bool isParented;

    public GameObject target_gameobject;
    public GameEvent gameEvent;

    public enum RewardMode
    {
        ADD_VALUE, SUBSTRACT_VALUE, SET_VALUE, DESTROY_GAMEOBJECT, SPAWN_GAMEOBJECT, TRIGGER_EVENT 
    }

    public RewardMode rewardMode;

	public void Claim()
    {
        switch (rewardMode)
        {
            case RewardMode.ADD_VALUE:
                if (isConstant)
                {
                    targetValue.value += delta_value;
                }
                else
                {
                    targetValue.value += delta_scriptableValue.value;
                }
                break;
            case RewardMode.SUBSTRACT_VALUE:
                if (isConstant)
                {
                    targetValue.value -= delta_value;
                }
                else
                {
                    targetValue.value -= delta_scriptableValue.value;
                }
                break;
            case RewardMode.DESTROY_GAMEOBJECT:
                Destroy(target_gameobject);
                break;
            case RewardMode.SPAWN_GAMEOBJECT:
                if (isParented)
                {
                    Instantiate(target_gameobject, spawnPos, Quaternion.identity);
                }
                else
                {
                    Instantiate(target_gameobject, spawnPos, Quaternion.identity, parent);
                }
                break;
            case RewardMode.TRIGGER_EVENT:
                gameEvent.Raise();
                break;
            case RewardMode.SET_VALUE:
                if (isConstant)
                {
                    targetValue.value = delta_value;
                }
                else
                {
                    targetValue.value = delta_scriptableValue.value;
                }
                break;
        }

        onRewardClaimed.Invoke();
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(Reward))]
public class RewardEditor: Editor
{
    Reward myTarget;

    private void OnEnable()
    {
        myTarget = (Reward)target;
    }

    public override void OnInspectorGUI()
    {
        myTarget.rewardMode = (Reward.RewardMode)EditorGUILayout.EnumPopup(myTarget.rewardMode);

        switch (myTarget.rewardMode)
        {
            case Reward.RewardMode.ADD_VALUE:
                PaintValue("+");
                break;
            case Reward.RewardMode.SUBSTRACT_VALUE:
                PaintValue("-");
                break;
            case Reward.RewardMode.SET_VALUE:
                PaintValue("=");
                break;
            case Reward.RewardMode.DESTROY_GAMEOBJECT:
                myTarget.target_gameobject = (GameObject)EditorGUILayout.ObjectField(myTarget.targetValue, typeof(GameObject), true);
                break;
            case Reward.RewardMode.SPAWN_GAMEOBJECT:
                myTarget.target_gameobject = (GameObject)EditorGUILayout.ObjectField(myTarget.targetValue, typeof(GameObject), false);
                break;
            case Reward.RewardMode.TRIGGER_EVENT:
                myTarget.gameEvent = (GameEvent)EditorGUILayout.ObjectField(myTarget.gameEvent, typeof(GameEvent), true);
                break;
        }
    }

    public void PaintValue(string icon)
    {
        myTarget.isConstant = EditorGUILayout.Toggle(myTarget.isConstant);
        EditorGUILayout.BeginHorizontal();
        myTarget.targetValue = (FloatValue)EditorGUILayout.ObjectField(myTarget.targetValue, typeof(FloatValue), false, GUILayout.Width(100));

        EditorGUILayout.LabelField(icon, GUILayout.Width(20));

        if (myTarget.isConstant)
        {
            myTarget.delta_value = EditorGUILayout.FloatField(myTarget.delta_value, GUILayout.Width(100));
        }
        else
        {
            myTarget.delta_scriptableValue = (FloatValue)EditorGUILayout.ObjectField(myTarget.delta_scriptableValue, typeof(FloatValue), false, GUILayout.Width(100));
        }

        EditorGUILayout.EndHorizontal();
    }
}
#endif