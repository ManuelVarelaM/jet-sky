﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : MonoBehaviour
{
    public bool onLand;
    public bool onWater;
    public float jumpForce;

    private Rigidbody body;

    private void Start()
    {
        body = this.GetComponent<Rigidbody>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        print("collision");

        Jumper jumper = collision.gameObject.GetComponent<Jumper>();
        if (jumper != null)
        {
            print("jump");
            StartJump(jumper.force);
        }

        if (collision.gameObject.layer == 8)
        {
            print("not jump");
            onLand = true;
            onWater = true;
        }
    }

    public void StartJump(float force)
    {
        body.AddForce(new Vector3(0, 0, -jumpForce * force));
    }
}
