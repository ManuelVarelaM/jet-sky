﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moving : MonoBehaviour
{
    public FloatValue timeSpeed;

    void FixedUpdate()
    {
        transform.position += new Vector3(0, JetSky.speedY * timeSpeed.value, 0);
    }
}
