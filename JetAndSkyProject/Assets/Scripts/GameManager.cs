﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public PauseManager pause;
    public FloatValue timeSpeedFloat;
    public static float timeSpeed;

    void Update()
    {
        timeSpeed = timeSpeedFloat.value * Time.fixedDeltaTime;

        if (Control.start)
        {
            pause.gameObject.SetActive(true);
            pause.Pause();
        }
    }
}
