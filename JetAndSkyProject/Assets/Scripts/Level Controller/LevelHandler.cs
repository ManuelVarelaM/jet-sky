﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelHandler : MonoBehaviour
{
    public bool startOnAwake = true;

    [SerializeField] LevelController lvlController;

    private bool isStarted;
    private int index;

	void Awake () {
        if (startOnAwake)
            Play();
	}
	
	void Update () {
        if (isStarted)
        {
            if (index < lvlController.waves.Count)
            {
                if (!lvlController.waves[index].isOver)
                {
                    lvlController.waves[index].Read();
                }
                else
                {
                    index++;
                }
            }
        }
	}

    public void Play()
    {
        if (lvlController != null && !isStarted)
        {
            if (lvlController.deselectOnRead)
            {
                foreach (LevelController.LevelGoal lg in lvlController.waves)
                {
                    lg.isOver = false;
                }
            }

            isStarted = true;
        }
    }
}
