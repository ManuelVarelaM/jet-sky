﻿using UnityEngine;
using System.Collections;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(menuName = "Jet&Sky/Level Controller/Formations/Create")]
public class Formation : ScriptableObject
{
    public FormationTile[] tiles = new FormationTile[11];

    [Serializable]
    public class FormationTile
    {
        public Entity entity;
        public Texture2D icon;
        public int index;

        public FormationTile(Entity entity, Texture2D icon, int index)
        {
            this.entity = entity;
            this.icon = icon;
            this.index = index;
        }
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(Formation))]
public class FormationEditor: Editor
{
    Formation myTarget;

    private void OnEnable()
    {
        myTarget = (Formation)target;
    }

    public override void OnInspectorGUI()
    {
        EditorGUILayout.BeginHorizontal();
        
        for (int i = 0; i < 11; i++)
        {
            if (i == 0)
            {
                DrawBox(i);

                EditorGUILayout.LabelField("", GUI.skin.verticalSlider, GUILayout.Width(5));
                EditorGUILayout.Space();
            }else if (i == 10)
            {
                EditorGUILayout.LabelField("", GUI.skin.verticalSlider, GUILayout.Width(5));
                EditorGUILayout.Space();
                DrawBox(i);
            }
            else
            {
                DrawBox(i);
            }
        }
        
        EditorGUILayout.EndHorizontal();

        EditorUtility.SetDirty(myTarget);
    }

    private void DrawBox(int index)
    {
        GUIStyle style = new GUIStyle("box");

        if (myTarget.tiles[index] != null)
        {
            if (myTarget.tiles[index].entity != null)
            {
                style.normal.background = myTarget.tiles[index].icon;
            }
        }

        EditorGUILayout.LabelField("", style, GUILayout.Width(20));

        if (GUILayoutUtility.GetLastRect().Contains(Event.current.mousePosition))
        {
            if (Event.current.button == 1)
            {
                myTarget.tiles[index] = null;
                return;
            }

            if (Event.current.type == EventType.DragUpdated)
            {
                if (DragAndDrop.objectReferences.Length > 0)
                {
                    if (DragAndDrop.objectReferences[0] is GameObject)
                    {
                        GameObject draggedObject = DragAndDrop.objectReferences[0] as GameObject;

                        if (draggedObject.GetComponent<Entity>())
                        {
                            DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
                        }
                        else
                        {
                            DragAndDrop.visualMode = DragAndDropVisualMode.Rejected;
                        }
                    }
                }

                Event.current.Use();
            }

            else if (Event.current.type == EventType.DragPerform)
            {
                if (DragAndDrop.objectReferences.Length > 0)
                {
                    if (DragAndDrop.objectReferences[0] is GameObject)
                    {
                        GameObject draggedObject = DragAndDrop.objectReferences[0] as GameObject;

                        if (draggedObject.GetComponent<Entity>())
                        {
                            myTarget.tiles[index] = new Formation.FormationTile(draggedObject.GetComponent<Entity>(), AssetPreview.GetAssetPreview(DragAndDrop.objectReferences[0]) ,index);
                        }
                    }
                }

                Event.current.Use();
                return;
            }

            if (Event.current.type == EventType.MouseDown)
            {
                if (myTarget.tiles[index].entity != null)
                {
                    EditorGUIUtility.PingObject(myTarget.tiles[index].entity);
                    return;
                }

            }
        }
    }
}
#endif

