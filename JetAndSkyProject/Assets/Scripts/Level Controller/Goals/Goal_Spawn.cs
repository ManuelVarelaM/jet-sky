﻿using UnityEngine;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(menuName = "Jet&Sky/Level Controller/Goals/Spawn")]
public class Goal_Spawn : Goal
{
    public enum Side { UP, DOWN, BOTH }

    public enum SpawnType{ HORDE, PROP }

    public enum Land { GROUND, WATER, BOTH }

    public enum Rail { LEFT, RIGHT, BOTH }

    public Side side;
    public Land land;
    public Rail rail;

    public float delayedTime;

    public float minArea, maxArea;

    public List<Formation> formations = new List<Formation>();
    public List<PropEntity> props = new List<PropEntity>();

    public bool isEndless;

    public float amountMin, amountMax;

    public float rateMin, rateMax;
    public float dropsMin, dropsMax;

    public SpawnType spawnType;

    private float spawningTime;
    private int currentFormation;

    public override bool Reading()
    {
        if (currentFormation < formations.Count)
        {
            if (Time.time >= spawningTime)
            {
                int index = 0;

                foreach (Formation.FormationTile tile in formations[currentFormation].tiles)
                {
                    if (tile.entity != null)
                    {
                        Vector3 spawnPos;

                        if (index == 0)
                        {
                            spawnPos = new Vector3(-4f, 3f, 0);
                        }
                        else if (index == 11)
                        {
                            spawnPos = new Vector3(4, 3f, 0);
                        }
                        else
                        {
                            float minPos = -2.75f, maxPos = 2.75f;
                            int tiles = 10;

                            float distance = -minPos + maxPos;

                            float deltaDistance = distance / tiles;

                            float nextPos = minPos + deltaDistance * index;

                            spawnPos = new Vector3(nextPos, 3f, 0);
                        }

                        Instantiate(tile.entity, spawnPos, Quaternion.identity);
                    }
                    index++;
                }

                spawningTime = Time.time + delayedTime;
                currentFormation++;
            }

            return false;
        }

        currentFormation = 0;
        spawningTime = 0;
        return true;
    }
}

#if UNITY_EDITOR

[CustomEditor(typeof(Goal_Spawn))]
public class GoalSpawnEditor : Editor
{
    Goal_Spawn myTarget; 

    private void OnEnable()
    {
        myTarget = (Goal_Spawn)target;
    }

    public override void OnInspectorGUI()
    {
        myTarget.side = (Goal_Spawn.Side)EditorGUILayout.EnumPopup("Side", myTarget.side);
        //myTarget.land = (Goal_Spawn.Land)EditorGUILayout.EnumPopup("Land", myTarget.land);

        switch (myTarget.land)
        {
            case Goal_Spawn.Land.GROUND:
                //PaintGroundOptions();
                break;
            case Goal_Spawn.Land.WATER:
                PaintWaterOptions();
                break;
            case Goal_Spawn.Land.BOTH:
                //PaintGroundOptions();
                PaintWaterOptions();
                break;
        }

        myTarget.spawnType = (Goal_Spawn.SpawnType)EditorGUILayout.EnumPopup("Type", myTarget.spawnType);

        switch (myTarget.spawnType)
        {
            case Goal_Spawn.SpawnType.HORDE:

                for (int i = 0; i < myTarget.formations.Count; i++)
                {
                    EditorGUILayout.BeginHorizontal();

                    myTarget.formations[i] = (Formation)EditorGUILayout.ObjectField("Formation " + i, myTarget.formations[i],typeof(Formation), false);

                    if (GUILayout.Button("Remove"))
                    {
                        myTarget.formations.Remove(myTarget.formations[i]);
                        return;
                    }

                    EditorGUILayout.EndHorizontal();
                }

                if (GUILayout.Button("+"))
                {
                    myTarget.formations.Add(null);
                    return;
                }

                myTarget.delayedTime = EditorGUILayout.Slider("Delayed Time", myTarget.delayedTime, 0, 60);
               // myTarget.isEndless = EditorGUILayout.Toggle("Endless", myTarget.isEndless);

                break;
            case Goal_Spawn.SpawnType.PROP:
                break;
        }

        /*
        if(myTarget.spawnType == Goal_Spawn.SpawnType.PROP || !myTarget.isEndless)
        {
            EditorGUILayout.LabelField("Amount");
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(myTarget.amountMin.ToString("F0"), GUI.skin.textField, GUILayout.Width(30));
            EditorGUILayout.MinMaxSlider(ref myTarget.amountMin, ref myTarget.amountMax, 0, 30, GUILayout.Width(180));
            EditorGUILayout.LabelField(myTarget.amountMax.ToString("F0"), GUI.skin.textField, GUILayout.Width(30));
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space();
        }


        EditorGUILayout.LabelField("Rate");
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(myTarget.rateMin.ToString("F0"), GUI.skin.textField, GUILayout.Width(20));
        EditorGUILayout.MinMaxSlider(ref myTarget.rateMin, ref myTarget.rateMax, 0, 60, GUILayout.Width(200));
        EditorGUILayout.LabelField(myTarget.rateMax.ToString("F0"), GUI.skin.textField, GUILayout.Width(20));
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.Space();

        EditorGUILayout.LabelField("Drops");
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(myTarget.dropsMin.ToString("F0"), GUI.skin.textField, GUILayout.Width(20));
        EditorGUILayout.MinMaxSlider(ref myTarget.dropsMin, ref myTarget.dropsMax, 0, 30, GUILayout.Width(200));
        EditorGUILayout.LabelField(myTarget.dropsMax.ToString("F0"), GUI.skin.textField, GUILayout.Width(20));
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.Space();
        */

        if (GUI.changed)
            EditorUtility.SetDirty(myTarget);
    }

    private void PaintGroundOptions()
    {
        myTarget.rail = (Goal_Spawn.Rail)EditorGUILayout.EnumPopup("Rail", myTarget.rail);
    }


    private void PaintWaterOptions()
    {
        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Area");
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(myTarget.minArea.ToString("F1"), GUI.skin.textField, GUILayout.Width(20));
        EditorGUILayout.MinMaxSlider(ref myTarget.minArea, ref myTarget.maxArea, 0, 1, GUILayout.Width(200));
        EditorGUILayout.LabelField(myTarget.maxArea.ToString("F1"), GUI.skin.textField, GUILayout.Width(20));
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.Space();
    }
}

#endif