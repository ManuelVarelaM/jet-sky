﻿using UnityEngine;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditorInternal;
#endif

[CreateAssetMenu(menuName = "Jet&Sky/Level Controller/Goals/Rings")]
public class Goal_Ring : Goal
{
    public List<RingGoal> rings = new List<RingGoal> ();
    public FinishRingZone finishRingZone_prefab;

    private int ringIndex;
    private float delayTime;
    private bool waiting;
    private bool isInit;

    private RingZone ringZone;

    [System.Serializable]
    public class RingGoal
    {
        public bool isDelay;
        public float delayTime;
        public Formation formation;
    }

    public override bool Reading()
    {
        if (!isInit)
        {
            isInit = true;
            ringZone = new RingZone();
        }

        if(ringIndex < rings.Count)
        {
            if (rings[ringIndex].isDelay)
            {
                if (!waiting)
                {
                    waiting = true;
                    delayTime = Time.time + rings[ringIndex].delayTime;
                }

                if (Time.time > delayTime)
                {
                    waiting = false;
                    ringIndex++;
                    return false;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                int index = 0;

                foreach (Formation.FormationTile tile in rings[ringIndex].formation.tiles)
                {
                    if (tile.entity != null)
                    {
                        Vector3 spawnPos;

                        if (index == 0)
                        {
                            spawnPos = new Vector3(-3.3f, 2.5f, 0);
                        }
                        else if (index == 11)
                        {
                            spawnPos = new Vector3(3.3f, 2.5f, 0);
                        }
                        else
                        {
                            float minPos = -2.3f, maxPos = 2.3f;
                            int tiles = 10;

                            float distance = -minPos + maxPos;

                            float deltaDistance = distance / tiles;

                            float nextPos = minPos + deltaDistance * index;

                            spawnPos = new Vector3(nextPos, 2.5f, 0);
                        }

                        Entity entity = Instantiate(tile.entity, spawnPos, Quaternion.identity);

                        if (entity is Ring)
                        {
                            Ring ring = entity as Ring;

                            ring.ringZone = ringZone;
                            ring.transform.position = new Vector3(ring.transform.position.x, ring.transform.position.y, tile.entity.transform.position.z);
                        }
                    }

                    index++;
                }

                ringIndex++;
                return false;
            }
        }

        Instantiate(finishRingZone_prefab, new Vector3(0, 2.5f, 0), Quaternion.identity);


        ringIndex = 0;
        isInit = false;
        return true;
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(Goal_Ring))]
public class Goal_RingEditor : Editor
{
    private SerializedProperty m_endzone;
    private ReorderableList list;

    private void OnEnable()
    {
        m_endzone = serializedObject.FindProperty("finishRingZone_prefab");

        list = new ReorderableList(serializedObject,
                serializedObject.FindProperty("rings"),
                true, true, true, true);

        list.drawElementCallback =
        (Rect rect, int index, bool isActive, bool isFocused) => {
            var element = list.serializedProperty.GetArrayElementAtIndex(index);
            rect.y += 2;

            EditorGUI.PropertyField(
                new Rect(rect.x, rect.y, 50, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("isDelay"), GUIContent.none);

            if (element.FindPropertyRelative("isDelay").boolValue)
            {
                element.FindPropertyRelative("delayTime").floatValue = EditorGUI.Slider(new Rect(rect.x + 60 , rect.y, 200, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("delayTime").floatValue, 0f, 3f);
            }
            else
            {
                EditorGUI.PropertyField(
               new Rect(rect.x + 60, rect.y, 200, EditorGUIUtility.singleLineHeight),
               element.FindPropertyRelative("formation"), GUIContent.none);
            }

            
        };
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        EditorGUILayout.PropertyField(m_endzone, new GUIContent("End zone"), GUILayout.Height(20));
        list.DoLayoutList();
        serializedObject.ApplyModifiedProperties();

        if (GUI.changed) EditorUtility.SetDirty(target); 
    }
}
#endif