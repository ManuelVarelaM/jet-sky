﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PixelArtAnimationSheet))]
public class PixelArtAnimatorEditor : Editor {

    PixelArtAnimationSheet myTarget;
    private Texture2D[] textures;

    int columns;
    int rows;

    private void OnEnable()
    {
        myTarget = (PixelArtAnimationSheet)target;

        columns = myTarget.Columns;
        rows = myTarget.Rows;

        Sprite[] sprites = myTarget.GetAnimationSheet();
        textures = new Texture2D[myTarget.Columns * myTarget.Rows];

        for (int c = 0; c < myTarget.Columns; c++)
        {
            for (int r = 0; r < myTarget.Rows; r++)
            {
                //textures[c, r] = TextureFromSprite(sprites[c,r]);

                if(sprites[c + r * myTarget.Columns] != null)
                {
                    textures[c + r * myTarget.Columns] = sprites[c + r * myTarget.Columns].texture;
                }
                else
                {
                    textures[c + r * myTarget.Columns] = null;
                }
            }
        }
    }

    public override void OnInspectorGUI()
    {
        columns = EditorGUILayout.IntField("Columns", columns);
        rows = EditorGUILayout.IntField("Rows", rows);

        int focusPicker = GUIUtility.GetControlID(FocusType.Passive) + 100;

        int size = 50;

        for (int c = 0; c < myTarget.Columns; c++)
        {
            EditorGUILayout.BeginHorizontal();

            for (int r = 0; r < myTarget.Rows; r++)
            {
                Rect slot = new Rect(size * c + 10 , size * r + 120, size, size);
                myTarget.GetAnimationSheet()[c + r * myTarget.Columns] = (Sprite)EditorGUI.ObjectField(slot, myTarget.GetAnimationSheet()[c + r * myTarget.Columns], typeof(Sprite), false);
            }

            EditorGUILayout.EndHorizontal();
        }

        if (GUILayout.Button("Update"))
        {
            myTarget.SetSize(columns, rows);
            textures = new Texture2D[columns * rows];

            EditorUtility.SetDirty(myTarget);
        }
    }

    private Texture2D TextureFromSprite(Sprite sprite)
    {
        if (sprite.rect.width != sprite.texture.width)
        {
            Texture2D newText = new Texture2D((int)sprite.rect.width, (int)sprite.rect.height);
            Color[] newColors = sprite.texture.GetPixels((int)sprite.textureRect.x,
                                                         (int)sprite.textureRect.y,
                                                         (int)sprite.textureRect.width,
                                                         (int)sprite.textureRect.height);
            newText.SetPixels(newColors);
            newText.Apply();
            return newText;
        }
        else
            return sprite.texture;
    }
}
