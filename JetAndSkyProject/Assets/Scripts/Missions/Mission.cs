﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "JetSky/Mission")]

public class Mission : ScriptableObject
{
    public string name;
    public string description;
    public LevelController level;

    void Start()
    {
        
    }
    
    void Update()
    {
        
    }
}
