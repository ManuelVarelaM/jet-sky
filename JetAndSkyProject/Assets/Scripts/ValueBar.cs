﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ValueBar : MonoBehaviour {

    [SerializeField]
    private FloatValue value;

    [SerializeField]
    private Image value_image;

    private float lastValue;

    public bool isAnimated;
    public float animationSpeed = 1;

    private void Start()
    {
        lastValue = value.value;
        UpdateValue();
    }

    public void UpdateValue()
    {
        if (isAnimated)
        {
            StartCoroutine(ChangeValue());
        }
        else
        {
            value_image.fillAmount = value.value;
            lastValue = value.value;
        }
    }

    private IEnumerator ChangeValue()
    {
        if (lastValue < value.value)
        {
            while (lastValue < value.value)
            {
                lastValue += Time.deltaTime * animationSpeed;
                value_image.fillAmount = lastValue;
                yield return new WaitForSeconds(Time.deltaTime);
            }

            lastValue = value.value;
            value_image.fillAmount = lastValue;
        }
        else
        {
            while (lastValue > value.value)
            {
                lastValue -= Time.deltaTime * animationSpeed;
                value_image.fillAmount = lastValue;
                yield return new WaitForSeconds(Time.deltaTime);
            }

            lastValue = value.value;
            value_image.fillAmount = lastValue;
        }
    }
	
}
