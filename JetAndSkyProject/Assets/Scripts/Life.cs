﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;

#endif

public class Life : MonoBehaviour
{
    public FloatValue optionalSharedValue;
    private float currentLife = 12;
    [SerializeField]
    [Range(0,100)]
    private float maxLife = 12;

    [Space]

    [SerializeField]
    private UnityEvent onTakeDamage;
    [SerializeField]
    private UnityEvent onRestoreLife;
    [SerializeField]
    private UnityEvent onLifeChangued;
    [SerializeField]
    private UnityEvent onKill;

    private void Start()
    {
        currentLife = maxLife;

        if (optionalSharedValue != null)
        {
            optionalSharedValue.value = GetCurrentLife();
        }

        onLifeChangued.Invoke();
    }

    public float GetMaxLife()
    {
        return maxLife;
    }

    public float GetCurrentLife()
    {
        return currentLife / maxLife;
    }

    public void Damage(float amount)
    {
        SubstractLife(amount);
    }

    public void SubstractLife(float amount)
    {
        currentLife -= amount;

        if (optionalSharedValue != null)
        {
            optionalSharedValue.value = GetCurrentLife();
        }

        onTakeDamage.Invoke();
        onLifeChangued.Invoke();

        if (currentLife <= 0)
            onKill.Invoke();

    }

    public void RestoreLife(float amount)
    {
        currentLife += amount;

        if (optionalSharedValue != null)
        {
            optionalSharedValue.value = GetCurrentLife();
        }

        onRestoreLife.Invoke();
        onLifeChangued.Invoke();

    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(Life))]
public class LifeEditor: Editor
{
    public override void OnInspectorGUI()
    {
        ProgressBar(((Life)target).GetCurrentLife(), (((Life)target).GetCurrentLife()*100).ToString("F0") + "%");

        base.OnInspectorGUI();
    }

    void ProgressBar(float value, string label)
    {
        // Get a rect for the progress bar using the same margins as a textfield:
        Rect rect = GUILayoutUtility.GetRect(18, 18, "TextField");
        EditorGUI.ProgressBar(rect, value, "Life " + label);
        EditorGUILayout.Space();
    }
}
#endif
