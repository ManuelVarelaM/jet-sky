﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(menuName = "Jet&Sky/Level Controller/Map/Sheet")]
public class MapSheet : ScriptableObject
{
    public List<MapSheetTile> mapTiles = new List<MapSheetTile>();

    public MapSheet nextMapSheet;
    public bool random;

    [Serializable]
    public class MapSheetTile
    {
        public GameObject prefab;
        public Texture2D icon;

        public MapSheetTile(GameObject prefab, Texture2D icon)
        {
            this.prefab = prefab;
            this.icon = icon;
        }
    }
}

/*

#if UNITY_EDITOR
[CustomEditor(typeof(MapSheet))]
public class MapTileEditor : Editor
{
    MapSheet myTarget;
    readonly int heightSize = 5;
    readonly int separation = 5;

    private void OnEnable()
    {
        myTarget = (MapSheet)target;
    }

    /*
    public override void OnInspectorGUI()
    {
        for (int i = 0; i < heightSize; i++)
        {
            EditorGUILayout.BeginHorizontal();

            for (int e = 0; e < separation; e++)
            {
                if (e == 0)
                {
                    DrawBox(i, ref myTarget.mapTiles);
                }
                else if (e == 1)
                {
                    EditorGUILayout.LabelField("", GUI.skin.verticalSlider, GUILayout.Width(5));
                    EditorGUILayout.Space();
                }
                else if(e == separation - 2)
                {
                    EditorGUILayout.LabelField("", GUI.skin.verticalSlider, GUILayout.Width(5));
                    EditorGUILayout.Space();
                }
                else if (e == separation - 1)
                {
                    DrawBox(i, ref myTarget.right_Column);
                }
                else
                {
                    EditorGUILayout.Space();
                }
            }

            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.Space();

        myTarget.loop = EditorGUILayout.Toggle("Is Loop", myTarget.loop);

        if (!myTarget.loop)
        {
            myTarget.nextMapSheet = (MapSheet)EditorGUILayout.ObjectField("Next Map Sheet", myTarget.nextMapSheet, typeof(MapSheet), false);
        }

        EditorUtility.SetDirty(myTarget);
    }

    private void DrawBox(int index, ref MapSheet.MapSheetTile[] side)
    {
        GUIStyle style = new GUIStyle("box");

        if (side[index] != null)
        {
            if (side[index].prefab != null)
            {
                style.normal.background = side[index].icon;
            }
        }

        EditorGUILayout.LabelField("", style, GUILayout.Width(20));

        if (GUILayoutUtility.GetLastRect().Contains(Event.current.mousePosition))
        {
            if (Event.current.button == 1)
            {
                side[index] = null;
                return;
            }

            if (Event.current.type == EventType.DragUpdated)
            {
                if (DragAndDrop.objectReferences.Length > 0)
                {
                    if (DragAndDrop.objectReferences[0] is GameObject)
                    {
                        GameObject draggedObject = DragAndDrop.objectReferences[0] as GameObject;

                        DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
                    }
                }

                Event.current.Use();
            }

            else if (Event.current.type == EventType.DragPerform)
            {
                if (DragAndDrop.objectReferences.Length > 0)
                {
                    if (DragAndDrop.objectReferences[0] is GameObject)
                    {
                        GameObject draggedObject = DragAndDrop.objectReferences[0] as GameObject;
                        side[index] = new MapSheet.MapSheetTile(draggedObject, AssetPreview.GetAssetPreview(DragAndDrop.objectReferences[0]));
                    }
                }

                Event.current.Use();
                return;
            }

            if (Event.current.type == EventType.MouseDown)
            {
                if (side[index].prefab != null)
                {
                    EditorGUIUtility.PingObject(side[index].prefab);
                    return;
                }

            }
        }
    }

}
    */
//#endif