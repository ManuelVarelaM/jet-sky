﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class MapScroller : MonoBehaviour
{
    const int mapTiles = 10;
    const float separation = 3.15f;
    public FloatValue levelWidth;

    public List<GameObject> maps;
    public List<GameObject> spawner;

    public MapSheet currentMapsLeft;
    public MapSheet currentMapsRight;
    public int mapIndexLeft = 0;
    public int mapIndexRight = 0;

    public GameObject lastMap;

    private Queue<MapSheet> nextMapsLeft;
    private Queue<MapSheet> nextMapsRight;

    public JetSky player;

    private void Awake()
    {
        //GameObject spawnerObject = new GameObject("Map Spawner");
        //LinearMovement movement = spawnerObject.AddComponent<LinearMovement>();
        //spawner = spawnerObject.transform;
        //spawner.position -= new Vector3(0, 6, 0);

        for (int i = 0; i < mapTiles; i ++)
        {
            AddMap();
        }
    }

    private void Update()
    {
        if (maps[0].transform.position.y < player.transform.position.y - 12)
        {
            Destroy(maps[0]);
            maps.RemoveAt(0);

            AddMap();
        }
    }

    public void AddMap()
    {
        GameObject mapTile;
        GameObject leftSide;
        GameObject rightSide;

        mapTile = new GameObject("mapTile");
        leftSide = Instantiate(currentMapsLeft.mapTiles[mapIndexLeft].prefab);
        rightSide = Instantiate(currentMapsRight.mapTiles[mapIndexLeft].prefab);

        if (mapIndexLeft + 1 >= currentMapsLeft.mapTiles.Count || currentMapsLeft.mapTiles[mapIndexLeft + 1] == null)
        {
            mapIndexLeft = 0;
            NextMapSheet();
        }

        else
        {
            mapIndexLeft++;
        }

        mapTile.transform.position = spawner[2].transform.position;
        leftSide.transform.position = spawner[0].transform.position;
        rightSide.transform.position = spawner[4].transform.position;
        rightSide.transform.localScale = new Vector3(-1, 1, 1);

        leftSide.transform.SetParent(mapTile.transform);
        rightSide.transform.SetParent(mapTile.transform);

        mapTile.transform.position = lastMap.transform.position + new Vector3(0, 3.2f, 0);
        mapTile.AddComponent<LinearMovement>();
        
        //leftSide.transform.position = newMapTransform.position - new Vector3(levelWidth.value + 3.5f, 0, 0);
        //rightSide.transform.position = newMapTransform.position + new Vector3(levelWidth.value + 3.5f, 0, 0);

        maps.Add(mapTile);
        lastMap = mapTile;
    }

    public void SetCurrentMapSheets(MapSheet newMapsLeft, MapSheet newMapsRight)
    {
        currentMapsLeft = newMapsLeft;
        currentMapsRight = newMapsRight;
    }

    public void AddMapSheetLeft(MapSheet nextMaps)
    {
        nextMapsLeft.Enqueue(nextMaps);
    }

    public void AddMapSheetRight(MapSheet nextMaps)
    {
        nextMapsRight.Enqueue(nextMaps);
    }

    public void NextMapSheet()
    {
        try
        {
            currentMapsLeft = nextMapsLeft.Dequeue();
        }

        catch
        {
            // No hay mapa siguiente
        }
        try
        {
            currentMapsRight = nextMapsRight.Dequeue();
        }

        catch
        {
            // No hay mapa siguiente
        }
    }
}
