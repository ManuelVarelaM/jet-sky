﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamageable
{
    float MaxLife { get; }
    float CurrentLife { get; }
    Transform Transform { get; }
    void GetDamage(float i);
}
