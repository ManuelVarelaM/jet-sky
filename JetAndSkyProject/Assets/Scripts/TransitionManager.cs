﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TransitionManager : MonoBehaviour
{
    public GameObject camera;

    public float phase = 0;
    public int countdown;

    public bool continueCount;
    public bool gameOver;
    public bool setSun;

    public GameObject sun;
    public GameObject waveImage;
    public GameObject continueCanvas;
    public GameObject continueImage;

    public Countdown countdownNumber;
    public Fade fade;

    // Start is called before the first frame update
    void Start()
    {
        waveImage.SetActive(false);

        if (continueCount)
        {
            fade.CancelFade();
            camera.transform.eulerAngles = new Vector3(10, 0, 0);
            continueCanvas.SetActive(true);

            countdown = 9;
            
            IEnumerator coroutine = ContinueCountdown();
            StartCoroutine(coroutine);
        }

        else
        {
            continueCanvas.SetActive(false);

            IEnumerator coroutine = StartAnimation();
            StartCoroutine(coroutine);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (continueCount)
        {
            if (Control.accept)
            {
                StopAllCoroutines();
                continueCount = false;
                phase = 1;
            }
        }

        if (setSun)
        {
            if (sun.transform.localPosition.y > -6)
            {
                sun.GetComponent<Move>().speedY = -0.05f;
            }

            else
            {
                sun.GetComponent<Move>().speedY = 0;
            }
        }

        if (phase == 1)
        {
            if (camera.transform.eulerAngles.x > 10)
            {
                camera.transform.Rotate(-0.4f, 0, 0);
            }
        }
    }

    public IEnumerator StartAnimation()
    {
        yield return new WaitForSeconds(2f);
        phase = 1;

        yield return new WaitForSeconds(2f);
        setSun = true;

        yield return new WaitForSeconds(4f);
        waveImage.SetActive(true);

        yield return new WaitForSeconds(2f);
        SceneManager.LoadScene("Game");
    }

    public IEnumerator ContinueCountdown()
    {
        yield return new WaitForSeconds(1);
        CountDown();
        yield return new WaitForSeconds(1);
        CountDown();
        yield return new WaitForSeconds(1);
        CountDown();
        yield return new WaitForSeconds(1);
        CountDown();
        yield return new WaitForSeconds(1);
        CountDown();
        yield return new WaitForSeconds(1);
        CountDown();
        yield return new WaitForSeconds(1);
        CountDown();
        yield return new WaitForSeconds(1);
        CountDown();
        yield return new WaitForSeconds(1);
        CountDown();

        IEnumerator coroutine = GameOver();
        StartCoroutine(coroutine);
    }

    public void CountDown()
    {
        countdown--;
        countdownNumber.SetNumber(countdown);
    }

    public IEnumerator GameOver()
    {
        continueImage.SetActive(false);

        countdownNumber.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.2f);
        countdownNumber.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.2f);

        countdownNumber.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.2f);
        countdownNumber.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.2f);

        countdownNumber.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.2f);
        countdownNumber.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.2f);

        countdownNumber.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.2f);
        countdownNumber.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.2f);

        countdownNumber.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.2f);
        countdownNumber.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.2f);

        countdownNumber.gameObject.SetActive(false);
        continueCanvas.SetActive(false);

        yield return new WaitForSeconds(1);
        fade.FadeToLayout("Menu");
    }
}
