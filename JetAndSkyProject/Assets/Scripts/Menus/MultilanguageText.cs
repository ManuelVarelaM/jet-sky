﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MultilanguageText : MonoBehaviour
{
    public List<string> texts;

    private TextMeshProUGUI text;

    void Start()
    {
        text = this.GetComponent<TextMeshProUGUI>();
        text.text = texts[(int) Options.language];
    }
    
    void Update()
    {
        text.text = texts[(int) Options.language];
    }
}
