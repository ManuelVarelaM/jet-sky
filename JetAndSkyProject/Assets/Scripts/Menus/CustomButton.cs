﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class CustomButton : MonoBehaviour
{
    public int index;
    public int imageID;

    public int lineColour;
    public float[] lineWidth;

    public bool selected = false;

    public UnityEvent action;

    private void Update()
    {
        UpdateImage();
    }

    public void SetSelected(bool selected)
    {
        this.selected = selected;
    }

    public void UpdateImage()
    {
        string imagePath = "menu";

        if (selected)
        {
            imagePath += "Selected";
            this.transform.localScale = new Vector3(1.3f, 1.3f, 1);
        }

        else
        {
            this.transform.localScale = new Vector3(1, 1, 1);
        }
        
        Sprite buttonSprite = Resources.LoadAll<Sprite>("2D Assets/Menu/" + imagePath + (Options.language.ToString()))[imageID];
        this.GetComponent<Image>().sprite = buttonSprite;
    }
}
