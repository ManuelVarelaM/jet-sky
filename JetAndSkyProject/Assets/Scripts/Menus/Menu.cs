﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    public int current;
    public List<CustomButton> buttons;
    public CustomButton selectedButton;
    public GameObject line;
    
    public bool selected;
    public IEnumerator coroutine;

    private void Start()
    {
        selected = false;
    }

    void Update()
    {
        selectedButton = buttons[current];

        for (int i = 0; i < buttons.Count; i ++)
        {
            if (i == current)
                buttons[i].SetSelected(true);

            else
                buttons[i].SetSelected(false);
        }

        // Linea
        line.transform.localPosition = new Vector3(selectedButton.transform.localPosition.x,
            Mathf.Lerp(line.transform.localPosition.y, selectedButton.transform.localPosition.y, 0.2f));

        float newWidth = Mathf.Lerp(line.transform.localScale.x, selectedButton.lineWidth[(int)Options.language], 0.2f);
        line.transform.localScale = new Vector3(newWidth, 1, 1);

        Sprite lineSprite = Resources.LoadAll<Sprite>("2D Assets/Menu/line")[selectedButton.lineColour];
        line.GetComponent<Image>().sprite = lineSprite;

        if (!selected)
        {
            if (Control.down & current < buttons.Count - 1)
            {
                current++;
                MainMenuManager.PlaySound("Menu/Menu arriba");
            }

            if (Control.up & current > 0)
            {
                current--;
                MainMenuManager.PlaySound("Menu/Menu arriba");
            }

            if (Control.accept)
            {
                coroutine = SelectButton(buttons[current]);
                StartCoroutine(coroutine);

                MainMenuManager.PlaySound("Menu/Enter Menu 1");
            }
        }
    }

    public void Activate()
    {
        selectedButton = buttons[current];
        foreach (CustomButton button in buttons)
        {
            button.UpdateImage();
        }

        line.transform.localPosition = new Vector3(selectedButton.transform.localPosition.x, selectedButton.transform.localPosition.y);
        line.transform.localScale = new Vector3(selectedButton.lineWidth[(int) Options.language], 1, 1);

        Sprite lineSprite = Resources.LoadAll<Sprite>("2D Assets/Menu/line")[selectedButton.lineColour];
        line.GetComponent<Image>().sprite = lineSprite;
    }

    public IEnumerator SelectButton(CustomButton button)
    {
        selected = true;
        
        yield return new WaitForSeconds(0.1f);
        button.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.1f);
        button.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        button.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.1f);
        button.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        button.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.1f);
        button.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.1f);

        button.action.Invoke();

        selected = false;
        button.gameObject.SetActive(true);
    }
}
