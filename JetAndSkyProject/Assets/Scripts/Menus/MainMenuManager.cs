﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuManager : MonoBehaviour
{
    public enum menu {intro, start, main, options, play, chapter};
    public menu menuState;

    public int musicPhase;

    public Image sunlight;
    public PressStart pressStart;
    public OptionsManager optionsPanel;

    public GameObject mainMenu;
    public GameObject playMenu;
    public GameObject optionsMenu;
    public GameObject intro;

    public Menu menuMain;
    public Menu menuPlay;

    public GameObject introJet;
    public GameObject introSky;
    public Fade fade;

    public static AudioSource audioSource;
    public AudioSource musicSource;

    private void Start()
    {
        SetMenu(menu.intro);

        audioSource = this.GetComponents<AudioSource>()[0];
        musicSource = this.GetComponents<AudioSource>()[1];

        PlayMusic("Menu0");
    }

    private void Update()
    {
        if (menuState == menu.intro)
        {
            if (introJet.transform.localPosition.x < -120)
            {
                Vector3 speed = new Vector3(12, 0, 0);

                introJet.transform.localPosition += speed;
                introSky.transform.localPosition -= speed;
            }

            else
            {
                intro.SetActive(false);
                Sunlight();
                SetMenu(menu.start);
            }
        }

        else if (menuState == menu.start)
        {
            if (Control.start)
            {
                IEnumerator coroutine = pressStart.PressedStart();
                StartCoroutine(coroutine);

                PlaySound("Menu/Start");
            }

            if (pressStart.start)
            {
                SetMenu(menu.main);
                PlayMusic("Menu2");
                musicPhase = 1;
            }
        }

        else if (menuState == menu.play)
        {
            if (Control.cancel)
            {
                SetMenu(menu.main);
            }
        }

        else if (menuState == menu.options)
        {
            if (Options.optionsOpen & !optionsPanel.active)
                optionsPanel.Activate();

            if ((!optionsPanel.selected & Control.cancel) || !Options.optionsOpen)
            {
                optionsPanel.Exit();
                SetMenu(menu.main);
                Options.optionsOpen = false;

                RectTransform rect = optionsPanel.GetComponent<RectTransform>();
                rect.localPosition = new Vector3(0, 480, 0);
            }
        }

        float alpha = sunlight.color.a - 0.01f;

        if (sunlight.color.a > 0)
            sunlight.color = new Color(255, 255, 255, alpha);

        MusicPhase();
    }

    public void SetMenu(menu newMenu)
    {
        menuState = newMenu;

        if (menuState == menu.intro)
        {
            intro.SetActive(true);
            mainMenu.SetActive(false);
            menuMain.gameObject.SetActive(false);
        }

        else if (menuState == menu.start)
        {
            mainMenu.SetActive(true);
            menuMain.gameObject.SetActive(false);
            playMenu.SetActive(false);
            optionsMenu.SetActive(false);
            intro.SetActive(false);
            pressStart.gameObject.SetActive(true);
        }

        else if (menuState == menu.main)
        {
            menuMain.Activate();

            mainMenu.SetActive(true);
            menuMain.gameObject.SetActive(true);
            playMenu.SetActive(false);
            optionsMenu.SetActive(false);
            pressStart.gameObject.SetActive(false);
        }

        else if (menuState == menu.play)
        {
            menuPlay.Activate();

            mainMenu.SetActive(false);
            menuMain.gameObject.SetActive(false);
            playMenu.SetActive(true);
            optionsMenu.SetActive(false);
            pressStart.gameObject.SetActive(false);
        }

        else if (menuState == menu.options)
        {
            mainMenu.SetActive(false);
            menuMain.gameObject.SetActive(false);
            playMenu.SetActive(false);
            optionsMenu.SetActive(true);
            pressStart.gameObject.SetActive(false);

            optionsPanel.Activate();
        }
    }

    public void SetMenu(string newMenu)
    {
        if      (newMenu == "start")    SetMenu(menu.start);
        else if (newMenu == "main")     SetMenu(menu.main);
        else if (newMenu == "options")  SetMenu(menu.options);
        else if (newMenu == "play")     SetMenu(menu.play);
        else if (newMenu == "chapter")  SetMenu(menu.chapter);
    }

    public void StartNewGame()
    {
        fade.FadeToLayout("Transition");
        //playMenu.SetActive(false);
    }

    public void Sunlight()
    {
        sunlight.gameObject.SetActive(true);
        sunlight.color = new Color(255, 255, 255, 1);
    }

    public void MusicPhase()
    {
        if (musicPhase == 0 & !musicSource.isPlaying)
        {
            PlayMusic("Menu1");
            musicSource.loop = true;
        }

        else if (musicPhase == 1 & !musicSource.isPlaying)
        {
            PlayMusic("Menu3");
            musicSource.loop = true;
        }
    }

    public static void PlaySound(string source)
    {
        audioSource.clip = Resources.Load<AudioClip>("Audio/SFX/" + source);
        audioSource.Play();
    }

    private void PlayMusic(string source)
    {
        musicSource.clip = Resources.Load<AudioClip>("Audio/Music/" + source);
        musicSource.Play();
    }
}
