﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PressStart : MonoBehaviour
{
    public IEnumerator coroutine;
    public bool start = false;

    public IEnumerator PressedStart()
    {
        yield return new WaitForSeconds(0.1f);
        this.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.1f);
        this.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        this.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.1f);
        this.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        this.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.1f);

        start = true;
    }
}
