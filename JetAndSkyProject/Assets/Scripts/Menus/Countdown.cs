﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Countdown : MonoBehaviour
{
    private Image image;

    private void Start()
    {
        image = this.GetComponent<Image>();
    }

    public void SetNumber(int i)
    {
        Sprite numberSprite = Resources.LoadAll<Sprite>("2D Assets/Menu/" + "countdown")[i];
        this.GetComponent<Image>().sprite = numberSprite;
    }
}
