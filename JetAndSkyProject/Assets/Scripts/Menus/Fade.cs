﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Fade : MonoBehaviour
{
    private Color color;
    private Image image;

    public bool fadeIn;
    public bool fadeOut;
    public string fadeToLayout;

    public float waitTime;
    public float fadeSpeed;

    // Start is called before the first frame update
    void Start()
    {
        image = GetComponent<Image>();
        color = image.color;

        if (fadeIn)
        {
            color.a = 1;
            image.color = color;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (fadeIn)
        {
            color.a -= fadeSpeed / 100;
            image.color = color;

            if (color.a <= 0)
            {
                fadeIn = false;
            }
        }

        else if (fadeOut)
        {
            color.a += fadeSpeed / 100;
            image.color = color;

            if (color.a >= 1)
            {
                fadeOut = false;

                if (fadeToLayout != "")
                {
                    SceneManager.LoadScene(fadeToLayout);
                }
            }
        }
    }

    public void FadeToLayout(string layout)
    {
        gameObject.SetActive(true);

        fadeOut = true;
        fadeToLayout = layout;
    }

    public void CancelFade()
    {
        fadeIn = false;
        color.a = 0;
        image.color = color;
        Debug.Log("fade cancelled");
    }
}
