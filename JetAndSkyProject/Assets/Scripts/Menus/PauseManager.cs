﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseManager : MonoBehaviour
{
    public bool optionsOpen;
    public Menu menu;
    public Image pause;
    public OptionsManager optionsPanel;
    public Image[] buttonsJet;
    public Image[] buttonsSky;

    void Start()
    {
    }
    
    void Update()
    {
        if (optionsOpen)
        {
            pause.rectTransform.localPosition = Vector3.Lerp(pause.rectTransform.localPosition, new Vector3(0, -110, 0), 0.1f);
            menu.gameObject.SetActive(false);

            if (!Options.optionsOpen)
            {
                optionsOpen = false;
            }
        }

        else
        {
            pause.rectTransform.localPosition = Vector3.Lerp(pause.rectTransform.localPosition, new Vector3(0, 50, 0), 0.1f);
            menu.gameObject.SetActive(true);
        }

        // Botones
        Sprite[] spritesJetButtons;
        Sprite[] spritesSkyButtons;

        if (Control.sharingControl)
        {
            spritesJetButtons = Resources.LoadAll<Sprite>("2D Assets/controls/buttonsSharedLeft");
            spritesSkyButtons = Resources.LoadAll<Sprite>("2D Assets/controls/buttonsSharedRight");
        }

        else
        {
            spritesJetButtons = Resources.LoadAll<Sprite>("2D Assets/controls/buttons" + optionsPanel.jetControlImageName);
            spritesSkyButtons = Resources.LoadAll<Sprite>("2D Assets/controls/buttons" + optionsPanel.skyControlImageName);
        }

        buttonsJet[0].GetComponent<Image>().sprite = spritesJetButtons[0];
        buttonsJet[1].GetComponent<Image>().sprite = spritesJetButtons[1];
        buttonsJet[2].GetComponent<Image>().sprite = spritesJetButtons[2];

        buttonsSky[0].GetComponent<Image>().sprite = spritesSkyButtons[0];
        buttonsSky[1].GetComponent<Image>().sprite = spritesSkyButtons[1];
        buttonsSky[2].GetComponent<Image>().sprite = spritesSkyButtons[2];
    }

    public void Pause()
    {
        this.gameObject.SetActive(true);
        menu.gameObject.SetActive(true);

        menu.current = 0;

        Time.timeScale = 0;
    }

    public void Unpause()
    {
        this.gameObject.SetActive(false);
        menu.gameObject.SetActive(false);

        Time.timeScale = 1;
    }

    public void OpenOptions()
    {
        optionsOpen = true;
        optionsPanel.Activate();
    }

    public void GoToMenu()
    {

    }
}
