﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Aim : MonoBehaviour
{
    public float speed;
    public Transform player;

    public void Update()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y, player.position.z);

        if (Control.sky != null)
            CircularAim();

        else
            ScreenAim();
    }

    private void ScreenAim()
    {
        Vector3 aimPos = new Vector3(Control.cursorX, Control.cursorY, 0);
        transform.position = Vector3.MoveTowards(transform.position, aimPos, speed);
    }

    private void CircularAim()
    {
        //Vector3 axispos;
        //axispos.normalized;
    }
}
