﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class GunController : MonoBehaviour
{
    [SerializeField]
    private FloatValue heat;

    [SerializeField]
    private GunCannon gunCannon;

    [SerializeField]
    private PixelArtAnimationSheet aimingSheet;

    [SerializeField]
    private Sprite[] _aim_sprites;

    [SerializeField]
    private GameEvent onHeatUpdate;

    [SerializeField]
    private GameEvent onHeatWeapon;

    [SerializeField]
    public float heatCoolDown = 1;
    private float heatCDTime;

    private SpriteRenderer _aim;
    private SpriteRenderer _pilot;

    private uint overheating_index;

    public float shootHeat = 1;
    private bool overheated;
    private float coolDownTime = 0.1f;

    public float rotationSpeed;
    private float angle;
    private float toAngle;

    public GameObject aim;

    private void Start()
    {
        _pilot = GetComponent<SpriteRenderer>();
    }

    private void FixedUpdate()
    {
        Vector3 dir = aim.transform.position;
        toAngle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;

        float deltaAngle = Mathf.DeltaAngle(angle, toAngle);

        if (deltaAngle > rotationSpeed / 2)
            angle += rotationSpeed;

        else if (deltaAngle < -rotationSpeed / 2)
            angle -= rotationSpeed;

        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

    private void Update()
    {
        if (!overheated)
        {
            if (Control.skyA)
            {
                coolDownTime -= Time.deltaTime;
                
                if (coolDownTime < 0)
                {
                    gunCannon.Shoot(transform.position, angle);

                    coolDownTime = 0.1f;
                    heat.value += shootHeat * Time.deltaTime;
                }

                if (heat.value >= 1)
                {
                    overheated = true;
                    onHeatWeapon.Raise();
                    heatCDTime = 0;
                }
            }

            else
            {
                heat.value -= Time.deltaTime;
            }
        }

        // Overheated
        else
        {
            if (heatCDTime > heatCoolDown)
            {
                if (heat.value > 0)
                {
                    heat.value -= Time.deltaTime;
                }

                else
                {
                    heat.value = 0;
                    overheated = false;
                }
            }

            else
            {
                heatCDTime += Time.deltaTime;
            }
        }

        Vector3 dir = aim.transform.position - transform.position;
        toAngle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;

        float deltaAngle = Mathf.DeltaAngle(angle, toAngle);

        if (deltaAngle > rotationSpeed / 2)
        {
            angle += rotationSpeed;
        }

        else if (deltaAngle < -rotationSpeed / 2)
        {
            angle -= rotationSpeed;
        }

        heat.value = Mathf.Clamp01(heat.value);

        onHeatUpdate.Raise();
    }

    private Sprite Aim_Sprite
    {
        get
        {
            return _aim_sprites[overheating_index];
        }
    }

    private Sprite Pilot
    {
        get
        {
            return _pilot.sprite;
        }
    }

    private SpriteRenderer Aim
    {
        get
        {
            return _aim;
        }
    }
}
