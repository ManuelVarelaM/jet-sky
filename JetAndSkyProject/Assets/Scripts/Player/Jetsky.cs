﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Life), typeof(Rigidbody))]
public class JetSky : Entity, IDamageable
{
    public FloatValue timeSpeed;
    public FloatValue dash;
    public FloatValue levelSpeed;

    public GameObject Jet;
    public GameObject Sky;

    public Jump jump;
    public GameEvent dashUpdate;
    public AudioSource audio;

    private Life _life;

    private Rigidbody _rg;
    public GameObject water_splash;

    public bool canDash = true;
    public bool dashing;
    public bool dashed = false;

    public Vector2 direction;
    
    public float localTime;
    public static float speedY;
    public static float movingSpeed;
    public static float cameraDistance;

    public float speedX;
    public float maxSpeedX;
    public float maxSpeedY;

    public float accelX;
    public float accelY;
    public float decelX;
    public float decelY;

    public float dashCooldown;
    private float dashTimer;

    private void Start()
    {
        audio = this.GetComponent<AudioSource>();
        jump = this.GetComponent<Jump>();
    }

    private void Update()
    {
        direction = new Vector3(Control.jetAxisX, Control.jetAxisY).normalized;
        localTime = timeSpeed.value * Time.fixedDeltaTime / 3;

        // Saltando
        if (!jump.onLand)
        {
            canDash = false;

            maxSpeedX = 20;
            maxSpeedY = 10;

            accelX = 1;
            accelY = 1;
            decelX = 0.2f;
            decelY = 0.2f;

            if (dashing)
                dashing = false;
        }

        // Sobre tierra
        else if (!jump.onWater)
        {
            canDash = false;

            maxSpeedX = 30;
            maxSpeedY = 25;

            accelX = 2;
            accelY = 2;
            decelX = 1;
            decelY = 1;
        }

        // Sobre agua
        else
        {
            // Mostrar splash de agua

            // Esta haciendo dash
            if (dashing)
            {
                maxSpeedX = 240;
                maxSpeedY = 80;

                accelX = 16;
                accelY = 16;
                decelX = 0;
                decelY = 0;

                dashTimer += localTime;

                // Termina el dash
                if (dashTimer >= dashCooldown)
                {
                    dashTimer = dashCooldown;
                    dashing = false;
                }
            }

            // No esta haciendo dash
            else
            {
                maxSpeedX = 80;
                maxSpeedY = 40;

                accelX = 4;
                accelY = 2;
                decelX = 2;
                decelY = 1;

                // Puede hacer dash?
                if (dashTimer > 0)
                {
                    dashTimer -= localTime / 5;
                }

                else
                {
                    dashTimer = 0;
                    canDash = true;
                    dashed = false;
                }
            }
        }

        maxSpeedX *= localTime;
        maxSpeedY *= localTime;

        accelX *= localTime;
        accelY *= localTime;
        decelX *= localTime;
        decelY *= localTime;
        
        // Aceleraciones
        speedX += accelX * direction.x;
        speedY += accelY * direction.y;

        // Correccion velocidad maxima X
        if (speedX > maxSpeedX)
            speedX = maxSpeedX;

        else if (speedX < - maxSpeedX)
            speedX = -maxSpeedX;

        // Correccion velocidad maxima Y
        if (speedY > maxSpeedY)
            speedY = maxSpeedY;

        else if (speedY < -maxSpeedY)
            speedY = -maxSpeedY;

        // Desaceleracion X
        if (direction.x == 0)
        {
            if (speedX > decelX)
                speedX -= decelX;

            else if (speedX < - decelX)
                speedX += decelX;

            else speedX = 0;
        }

        // Desaceleracion Y
        if (direction.y == 0)
        {
            if (speedY > decelY)
                speedY -= decelY;

            else if (speedY < - decelY)
                speedY += decelY;

            else speedY = 0;
        }

        // Dash
        if (Control.jetA & canDash & jump.onLand)
        {
            Dash();
        }

        // Fuerza de salto
        if (dashing)
            jump.jumpForce = 28;

        else
            jump.jumpForce = 500;
    }

    private void FixedUpdate()
    {
        Vector3 speed = new Vector3(Mathf.Round(speedX * 1000), Mathf.Round(speedY * 1000), 0);
        
        // 'rotate' indica el angulo hacia el que debe girar la moto de agua, segun su direccion x
        float rotate;

        // Hacia la izquierda
        if (direction.x < 0)
            rotate = 15;

        // Hacia la derecha
        else if (direction.x > 0)
            rotate = -15;

        else
            rotate = 0;

        transform.position = transform.position + new Vector3(speedX / 20, 0, 0);
        movingSpeed = levelSpeed.value + speedY * 2;
        cameraDistance = Mathf.Lerp(cameraDistance, speedY, 0.1f);
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, 0, rotate), 0.1f);

        if (jump.onWater)
            water_splash.SetActive(true);

        else
            water_splash.SetActive(false);

        dash.value = 1 - dashTimer / dashCooldown;
        dashUpdate.Raise();
    }

    private void Dash()
    {
        dashing = true;
        dashTimer = 0;
        canDash = false;
        dashed = true;

        PlaySound("splash");
    }

    public void PlaySound(string source)
    {
        audio.clip = Resources.Load<AudioClip>("Audio/SFX/" + source);
        audio.Play();
    }

    private void OnCollisionEnter(Collision collision)
    {

    }

    private Rigidbody RB
    {
        get
        {
            if (_rg == null)
                _rg = GetComponent<Rigidbody>();

            return _rg;
        }
    }

    public Life Life
    {
        get
        {
            if (_life == null)
                _life = GetComponent<Life>();

            return _life;
        }
    }

    public float MaxLife
    {
        get
        {
            return Life.GetMaxLife();
        }
    }

    public float CurrentLife
    {
        get
        {
            return Life.GetCurrentLife();
        }
    }

    public void GetDamage(float i)
    {
        Life.Damage(i);
    }

    public Transform Transform
    {
        get
        {
            return transform;
        }
    }
}
