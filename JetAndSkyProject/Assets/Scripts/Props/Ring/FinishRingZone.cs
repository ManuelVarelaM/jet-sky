﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider2D))]
public class FinishRingZone : MonoBehaviour
{
    public RingZone ringZone;

    public void FinishZone()
    {
        if (ringZone != null) ringZone.SavePoints();
        Destroy(this.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player") FinishZone();
    }
}
