﻿using UnityEngine;
using System.Collections;

public class LinearMovement : MonoBehaviour
{
    public enum Axis
    {
        HORIZONTAL,
        VERTICAL    
    }

    public Axis axis = Axis.VERTICAL;
    public float pingPongDistance = 5;
    public bool pingPong = false;
    public bool invertMovement = false;

    public void FixedUpdate()
    {
        if (axis == Axis.HORIZONTAL)
        {
            if (invertMovement)
            {
                transform.position -= transform.right * GameManager.timeSpeed;
            }
            else
            {
                transform.position += transform.right * GameManager.timeSpeed;
            }
        }
        else
        {
            if (invertMovement)
            {
                transform.position += transform.up * GameManager.timeSpeed * JetSky.movingSpeed;
            }
            else
            {
                transform.position -= transform.up * GameManager.timeSpeed * JetSky.movingSpeed;
            }
        }
    }
}
