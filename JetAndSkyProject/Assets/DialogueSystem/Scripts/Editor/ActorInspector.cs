﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Actor))]
public class ActorInspector : Editor
{
    Actor instance;

    private void OnEnable()
    {
        instance = (Actor)target;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        var i = 0;
        foreach (var emotion in instance.emotions)
        {
            emotion.name = EditorGUILayout.TextField("Name", emotion.name);
            
            EditorGUILayout.PropertyField(serializedObject.FindProperty("emotions").GetArrayElementAtIndex(i).FindPropertyRelative("iddle"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("emotions").GetArrayElementAtIndex(i).FindPropertyRelative("talking"), true);
            i++;

            if (GUILayout.Button("Remove Emotion"))
            {
                instance.emotions.Remove(emotion);
                return;
            }

            EditorGUILayout.Space();
        }

        if (GUILayout.Button("Add Emotion"))
        {
            instance.emotions.Add(new Actor.Emotion());
        }

        serializedObject.ApplyModifiedProperties();
    }
}
