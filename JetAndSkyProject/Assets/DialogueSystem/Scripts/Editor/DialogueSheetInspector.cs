﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(DialogueSheet))]
public class DialogueSheetInspector : Editor
{
    DialogueSheet instance;

    private void OnEnable()
    {
        instance = (DialogueSheet)target;
    }

    public override void OnInspectorGUI()
    {
        foreach (var dialogue in instance.dialogues)
        {
            EditorGUILayout.BeginVertical("window");

            EditorGUILayout.BeginHorizontal();
            //Content

            EditorGUILayout.BeginVertical();

            //Actor & Pos
            EditorGUILayout.BeginHorizontal();

            dialogue.actor = (Actor) EditorGUILayout.ObjectField(dialogue.actor, typeof(Actor), false);
            dialogue.position = (DialogueManager.Position)EditorGUILayout.EnumPopup(dialogue.position);

            EditorGUILayout.EndHorizontal();

            //Emotion & Talking mode
            EditorGUILayout.BeginHorizontal();

            if (dialogue.actor != null && dialogue.actor.emotions.Count > 0)
            {
                if (dialogue.emotion == null)
                {
                    if (GUILayout.Button("Select Emotion"))
                    {
                        SelectEmotion(dialogue);
                    }
                }
                else
                {
                    if (GUILayout.Button(dialogue.emotion.name))
                    {
                        SelectEmotion(dialogue);
                    }
                }
            }
            else
            {
                EditorGUILayout.LabelField("Actor slot it's empty!");
            }

            ///dialogue.emotion = (Actor)EditorGUILayout.ObjectField(dialogue.actor, typeof(Actor), false);
            //dialogue.position = (DialogueManager.Position)EditorGUILayout.EnumPopup(dialogue.position);
            EditorGUILayout.EndHorizontal();

            //----- Optional -----
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.EndHorizontal();

            //Dialogue & Id
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.EndHorizontal();

            //Sound

            EditorGUILayout.EndVertical();

            //Order

            EditorGUILayout.BeginVertical();

            if (GUILayout.Button("Up"))
            {

            }

            if (GUILayout.Button("Down"))
            {

            }

            EditorGUILayout.EndVertical();

            EditorGUILayout.EndHorizontal();
            
            EditorGUILayout.EndVertical();
        }
    }

    private void SelectEmotion(DialogueSheet.DialogueSlot slot)
    {
        GenericMenu menu = new GenericMenu();

        foreach (var emotion in slot.actor.emotions)
        {
            GUIContent content = new GUIContent(emotion.name);

            GenericMenu.MenuFunction onSelectEmotion = () => { slot.emotion = emotion; };

            if (slot.emotion != null)
            {
                if (slot.emotion.name == emotion.name)
                {
                    menu.AddItem(content, false, onSelectEmotion);
                }
                else
                {
                    menu.AddItem(content, false, onSelectEmotion);
                }
            }
            else
            {
                menu.AddItem(content, false, onSelectEmotion);
            }
        }

        menu.ShowAsContext();
    }
}
