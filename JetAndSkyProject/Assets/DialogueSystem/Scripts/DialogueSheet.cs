﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Jet&Sky/Dialogue Manager/Dialogue Sheet")]
public class DialogueSheet : ScriptableObject
{
    public List<DialogueSlot> dialogues = new List<DialogueSlot>();

    [System.Serializable]
    public class DialogueSlot
    {
        public Actor actor;
        public DialogueManager.Position position = DialogueManager.Position.UP_LEFT;
        public Actor.Emotion emotion;
        public bool isTalking;
        public Multilanguage dialogue;
        public string dialogue_id;
        public AudioClip clip;
    }
}
