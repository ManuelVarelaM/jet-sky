﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Jet&Sky/Dialogue Manager/Actor")]
public class Actor : ScriptableObject
{
    public List<Emotion> emotions = new List<Emotion>();

    [System.Serializable]
    public class Emotion
    {
        public string name;
        public Texture2D[] iddle;
        public Texture2D[] talking;
    }
}
