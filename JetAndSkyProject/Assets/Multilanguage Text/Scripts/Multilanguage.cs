﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Multilanguage")]
public class Multilanguage : ScriptableObject
{
    public List<Text> multilanguage_text = new List<Text>();

    [System.Serializable]
    public class Text
    {
        public string id = "ID";
        public List<string> texts = new List<string>();
    }
}
