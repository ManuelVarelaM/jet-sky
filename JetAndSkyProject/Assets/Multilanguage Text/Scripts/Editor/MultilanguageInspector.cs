﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Multilanguage))]
public class MultilanguageInspector : Editor
{
    Multilanguage instance;

    [SerializeField] List<TextSlot> slots = new List<TextSlot>();

    private void OnEnable()
    {
        instance = (Multilanguage)target;

        foreach (var slot in instance.multilanguage_text)
        {
            slots.Add(new TextSlot(slot));
        }
    }

    public override void OnInspectorGUI()
    {
        foreach (var textSlot in slots)
        {
            textSlot.foldout = EditorGUILayout.Foldout(textSlot.foldout, textSlot.texts.id);

            if (textSlot.foldout)
            {
                textSlot.texts.id = EditorGUILayout.TextField("Id", textSlot.texts.id);

                for (int i = 0; i < System.Enum.GetValues(typeof(Language)).Length; i++)
                {
                    try
                    {
                        GUILayout.Label(System.Enum.GetValues(typeof(Language)).GetValue(i).ToString());
                        textSlot.texts.texts[i] = EditorGUILayout.TextArea(textSlot.texts.texts[i], GUILayout.Height(64));

                        EditorGUILayout.Space();
                    }
                    catch
                    {
                        textSlot.texts.texts.Add("");
                        return;
                    }
                }

                if (GUILayout.Button("Remove text"))
                {
                    instance.multilanguage_text.Remove(textSlot.texts);
                    slots.Remove(textSlot);
                    return;
                }
            }
        }
        

        if (GUILayout.Button("Add text"))
        {
            var multi = new Multilanguage.Text();
            instance.multilanguage_text.Add(multi);
            slots.Add(new TextSlot(multi));
        }
    }

    [System.Serializable]
    private class TextSlot
    {
        public bool foldout = false;
        public Multilanguage.Text texts;

        public TextSlot(Multilanguage.Text text)
        {
            this.texts = text;
        }
    }
}
