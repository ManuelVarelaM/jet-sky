﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Shared Values/Float")]
public class FloatValue : ScriptableObject {
    public float value;
}
