﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Shared Values/Int")]
public class IntValue : ScriptableObject {
    public int value;
}
