﻿using UnityEngine;
using System.Collections;

public class CustomMono : MonoBehaviour
{
    protected virtual void Start()
    {

    }

    protected virtual void Update()
    {

    }

    protected virtual void OnTriggerEnter2D(Collider2D other)
    {

    }

    protected virtual void OnCollisionEnter2D(Collision2D collision)
    {
        
    }

    protected virtual void OnTriggerExit2D(Collider2D other)
    {

    }

    protected virtual void OnCollisionExit2D(Collision2D collision)
    {

    }

    protected virtual void OnTriggerStay2D(Collider2D collision)
    {

    }

    protected virtual void OnCollisionStay2D(Collision2D collision)
    {

    }
}
