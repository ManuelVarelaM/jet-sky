﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wave : PropEntity {

    [SerializeField]
    private GameEvent onWaveDash;

    [SerializeField]
    private ControllerTemplate template;

    private bool isOver;

    void Update()
    {
        if (template.leftController.Action() && isOver)
        {
            onWaveDash.Raise();
            isOver = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        isOver = false;

        if (other.tag == "Player")
        {
            isOver = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            isOver = false;
        }
    }
}
