﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class UIEffects : MonoBehaviour {

    [SerializeField]private Animator _anim;

    public enum Effects
    {
        SHAKE, HEAT
    }

    public Effects effect;

    public void Play()
    {
        switch (effect)
        {
            case Effects.SHAKE:
                _anim.SetTrigger("Shake");
                break;
            case Effects.HEAT:
                _anim.SetTrigger("Heat");
                break;
        }
    }
}
