﻿using UnityEngine;
using System.Collections;

public class MapScroller : MonoBehaviour
{
    public MapSheet currentSheet;

    public MapSector low_Sector;
    public MapSector medium_Sector;
    public MapSector hight_Sector;

    public MapSector currentHightSector;

    public Animator anim;

    public FloatValue speed;

    public float separation = 4f;
    public float widht = 4.3f;

    private void Awake()
    {
        BuildSector(currentSheet, ref low_Sector);
        BuildSector(currentSheet, ref medium_Sector);
        BuildSector(currentSheet, ref hight_Sector);
    }

    private void Update()
    {
        anim.SetFloat("Speed", speed.value);
    }

    public void ChangueMap(MapSheet newSheet)
    {
        if (!newSheet.isLoop)
        {
            BuildSector(newSheet, ref currentHightSector);

            currentHightSector.needRefresh = true;
        }
        else
        {
            currentSheet = newSheet;

            low_Sector.needRefresh = true;
            medium_Sector.needRefresh = true;
            hight_Sector.needRefresh = true;
        }
    }

    public void CheckLowSector()
    {
        if (low_Sector.needRefresh)
        {
            BuildSector(currentSheet, ref low_Sector);
        }

        currentHightSector = low_Sector;
    }

    public void CheckMediumSector()
    {
        if (medium_Sector.needRefresh)
        {
            BuildSector(currentSheet, ref medium_Sector);
        }

        currentHightSector = medium_Sector;
    }

    public void CheckHightSector()
    {
        if (hight_Sector.needRefresh)
        {
            BuildSector(currentSheet, ref hight_Sector);
        }

        currentHightSector = hight_Sector;
    }

    private void BuildSector(MapSheet sheet, ref MapSector sector)
    {
        for (int i = 0; i < 5; i++)
        {
            Vector3 pos = new Vector3(-widht, (separation * i), sheet.left_Column[i].prefab.transform.position.z);

            if (sheet.left_Column[i].prefab != null)
            {
                if (sector.layers[i].left != null) Destroy(sector.layers[i].left);
                sector.layers[i].left = Instantiate(sheet.left_Column[i].prefab, sector.transform);
                sector.layers[i].left.transform.localPosition = pos;
            }

            pos = new Vector3(widht, (separation * i), sheet.right_Column[i].prefab.transform.position.z);

            if (sheet.right_Column[i].prefab != null)
            {
                if (sector.layers[i].right != null) Destroy(sector.layers[i].right);
                sector.layers[i].right = Instantiate(sheet.right_Column[i].prefab, sector.transform);
                sector.layers[i].right.transform.localPosition = pos;
            }
        }

        sector.needRefresh = false;
    }

    [System.Serializable]
    public class MapSector
    {
        public Transform transform;
        public MapLayer[] layers = new MapLayer[5];

        public bool needRefresh;
    }

    [System.Serializable]
    public class MapLayer
    {
        public GameObject left;
        public GameObject right;
    }
}
