﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game Settings")]
public class Settings : ScriptableObject {

    public ControllerTemplate controller;
}
