﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu]
public class ControllerSide : ScriptableObject {

    public Sprite icon;

    public HorizontalAxisPresets horizontalAxis;
    public VerticalAxisPresets verticalAxis;

    public ControllerType controllerType;

    public KeyCode action;
    public KeyCode special;
    public MouseButton actionMouse;
    public MouseButton specialMouse;

    public enum HorizontalAxisPresets
    {
        Horizontal,
        HorizontalJoy1,
        HorizontalJoy2,
        MouseX
    }

    public enum VerticalAxisPresets
    {
        Vertical,
        VerticalJoy1,
        VerticalJoy2,
        MouseY
    }

    public enum ControllerType
    {
        KEYBOARD, CONTROLLER, MOUSE
    }

    public enum MouseButton
    {
        LEFT, RIGHT, SCROLL
    }
}
