﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu]
public class Controller : ScriptableObject
{
    [SerializeField]
    private ControllerSide side;

    public float Horizontal()
    {
        return Input.GetAxis(side.horizontalAxis.ToString());
    }

    public ControllerSide.ControllerType GetControllerType()
    {
        return side.controllerType;
    }

    public float Vertical()
    {
        return Input.GetAxis(side.verticalAxis.ToString());
    }

    public bool Action()
    {
        if (side.controllerType.Equals(ControllerSide.ControllerType.MOUSE))
        {
            return Input.GetMouseButton((int)side.actionMouse);
        }
        else
        {
            return Input.GetKeyDown(side.action);
        }
    }

    public bool Special()
    {
        if (side.controllerType.Equals(ControllerSide.ControllerType.MOUSE))
        {
            return Input.GetMouseButton((int)side.special);
        }
        else
        {
            return Input.GetKeyDown(side.special);
        }
    }
}
