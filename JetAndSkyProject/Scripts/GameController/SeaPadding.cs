﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeaPadding : MonoBehaviour {

    [SerializeField]
    private FloatValue speed;
    private float scrollSpeed = 0.5F;
    public Renderer rend;

    void OnEnable()
    {
        rend = GetComponent<Renderer>();
    }

    void Update()
    {
        float offset = Time.time * scrollSpeed * speed.value;
        rend.material.SetTextureOffset("_MainTex", new Vector2(0, -offset));
    }
}
