﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ControllerSide))]
public class GameControllerEditor : Editor {

    ControllerSide myTarget;

	void OnEnable()
    {
        myTarget = (ControllerSide)target;
    }

    public override void OnInspectorGUI()
    {
        EditorGUILayout.LabelField("Icon");
        myTarget.icon = (Sprite)EditorGUILayout.ObjectField(GUIContent.none, myTarget.icon, typeof(Sprite), allowSceneObjects: false, options: GUILayout.Width(55));

        EditorGUILayout.LabelField("Horizontal Axis");
        myTarget.horizontalAxis = (ControllerSide.HorizontalAxisPresets) EditorGUILayout.EnumPopup(myTarget.horizontalAxis);

        EditorGUILayout.LabelField("Vertical Axis");
        myTarget.verticalAxis = (ControllerSide.VerticalAxisPresets) EditorGUILayout.EnumPopup(myTarget.verticalAxis);

        EditorGUILayout.LabelField("Controller Type");
        myTarget.controllerType = (ControllerSide.ControllerType)EditorGUILayout.EnumPopup(myTarget.controllerType);

        if (myTarget.controllerType.Equals(ControllerSide.ControllerType.MOUSE))
        {
            EditorGUILayout.LabelField("Action Button");
            myTarget.actionMouse = (ControllerSide.MouseButton)EditorGUILayout.EnumPopup(myTarget.actionMouse);
            EditorGUILayout.LabelField("Special Button");
            myTarget.specialMouse = (ControllerSide.MouseButton)EditorGUILayout.EnumPopup(myTarget.specialMouse);
        }
        else
        {
            EditorGUILayout.LabelField("Action Button");
            myTarget.action = (KeyCode)EditorGUILayout.EnumPopup(myTarget.action);
            EditorGUILayout.LabelField("Special Button");
            myTarget.special = (KeyCode)EditorGUILayout.EnumPopup(myTarget.special);
        }

        if (GUI.changed)
        {
            EditorUtility.SetDirty(myTarget);
        }
    }
}
