﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu]
public class ControllerTemplate : ScriptableObject
{
    public Controller leftController;
    public Controller rightController;
}
