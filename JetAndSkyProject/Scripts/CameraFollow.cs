﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public Transform target;
    public float speed = 1;

    private void LateUpdate()
    {
        Vector3 middlePos = (target.position + Camera.main.ScreenToWorldPoint(Input.mousePosition)) / 2;
        middlePos.z = -8;
        transform.position = Vector3.MoveTowards(transform.position, middlePos, Time.smoothDeltaTime * speed);
    }
}
