﻿using UnityEngine;
using System.Collections.Generic;

public abstract class PropEntity : Entity
{
    public float maxLife;
    public float currentLife;
    public float resistance;
}
