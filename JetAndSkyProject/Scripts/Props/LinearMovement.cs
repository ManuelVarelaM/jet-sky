﻿using UnityEngine;
using System.Collections;

public class LinearMovement : MonoBehaviour
{
    public enum Axis
    {
        HORIZONTAL,
        VERTICAL    
    }

    public Axis axis = Axis.HORIZONTAL;
    public bool pingPong;
    public float pingPongDistance = 5;
    public FloatValue speed;
    public bool invertMovement;

    public void Update()
    {
        if (axis == Axis.HORIZONTAL)
        {
            if (invertMovement)
            {
                transform.position -= transform.right * speed.value * Time.deltaTime;
            }
            else
            {
                transform.position += transform.right * speed.value * Time.deltaTime;
            }
        }
        else
        {
            if (invertMovement)
            {
                transform.position += transform.up * speed.value * Time.deltaTime;
            }
            else
            {
                transform.position -= transform.up * speed.value * Time.deltaTime;
            }
        }
    }
}
