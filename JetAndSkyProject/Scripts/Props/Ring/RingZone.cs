﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class RingZone
{
    public int points;

    public void AddPoints(int points)
    {
        this.points += points;
    }

    public void SavePoints()
    {
    }
}
