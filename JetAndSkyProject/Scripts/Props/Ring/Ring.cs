﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider))]
public class Ring : PropEntity
{
    public RingZone ringZone;

    public int points;

    public void Reclaim()
    {
        if (ringZone != null) ringZone.AddPoints(points);

        Destroy(this.gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player") Reclaim();
    }
}
