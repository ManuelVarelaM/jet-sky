﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(menuName = "Jet&Sky/Ammo/Create")]
public class Ammo : ScriptableObject
{
    public int damage = 1;
    public float speed;

    public GameObject prefab;
    public GameObject impact_effect;

    public Bullet GetBullet()
    {
        GameObject instance = Instantiate(prefab);

        Bullet component = instance.AddComponent<Bullet>();

        component.Init(damage, speed, impact_effect);

        return component;
    }
}
