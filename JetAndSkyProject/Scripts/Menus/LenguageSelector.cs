﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LenguageSelector : MonoBehaviour {

    public int index = 0;
    public Image flagImage;
    public List<Sprite> flags;

    public void Update()
    {
        ChangeLenguage();
    }

    public void ChangeLenguage()
    {
        if(Input.GetButtonUp("Fire1"))
        {
            index++;
            if(index + 1 > flags.Count)
            {
                index = 0;
            }

            flagImage.sprite = flags[index];
        }
    }
}

public enum Lenguage
{
    SPANISH,
    ENGLISH,
    RUSSIAN
}