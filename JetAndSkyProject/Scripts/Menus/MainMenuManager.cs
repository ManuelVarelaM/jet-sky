﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class MainMenuManager : MonoBehaviour
{
    public Text startText;

    [SerializeField] ControllerTemplate controller;

    [SerializeField] UnityEvent onInitMenu;

    public void InitMenu()
    {
        onInitMenu.Invoke();
    }

    private void Update()
    {
        if (GetJoystickDevices() > 0)
        {
            startText.text = "Presiona <size=40>START</size> para Jugar";
        }
        else
        {
            startText.text = "Presiona <size=40>ENTER</size> para Jugar";
        }

        if (controller.leftController.Action() || controller.rightController.Action() || Input.GetKeyDown(KeyCode.Return))
        {
            InitMenu();
        }
    }

    private int GetJoystickDevices()
    {
        int devices = 0;

        for (int i = 0; i < Input.GetJoystickNames().Length; i++)
        {
            if (Input.GetJoystickNames()[i].Length > 0)
            {
                devices++;
            }
        }

        return devices;
    }
}
