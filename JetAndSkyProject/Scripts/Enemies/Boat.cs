﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boat : Enemy, IDamageable
{
    private static GameObject _player;
    public float distanceFromPlayer = 1.5f;
    public float speed = 0.1f;

    private static GameObject Player
    {
        get
        {
            if (_player == null)
                _player = GameObject.FindGameObjectWithTag("Player");

            return _player;
        }
    }

    private Life _life;

    public Life Life
    {
        get
        {
            if (_life == null)
                _life = GetComponent<Life>();

            return _life;
        }
    }

    public float MaxLife
    {
        get
        {
            return Life.GetMaxLife();
        }
    }

    public float CurrentLife
    {
        get
        {
            return Life.GetCurrentLife();
        }
    }

    public Transform Transform
    {
        get
        {
            return transform;
        }
    }

    public void GetDamage(float i)
    {
        Life.Damage(i);
    }

    private void FixedUpdate()
    {
        //Right of player
        if (transform.position.x > Player.transform.position.x)
        {
            RaycastHit hit;

            if (Physics.Raycast(transform.position, Vector2.right, out hit))
            {
                print(Mathf.Abs(hit.point.x - transform.position.x));
            }

            Vector2 pos = new Vector2(Player.transform.position.x + distanceFromPlayer, Player.transform.position.y);
            transform.position = Vector2.MoveTowards(transform.position, pos, Time.fixedDeltaTime * speed);
        }
        else//Left of player
        {
            Vector2 pos = new Vector2(Player.transform.position.x - distanceFromPlayer, Player.transform.position.y);
            transform.position = Vector2.MoveTowards(transform.position, pos, Time.fixedDeltaTime * speed);
        }
    }
}
