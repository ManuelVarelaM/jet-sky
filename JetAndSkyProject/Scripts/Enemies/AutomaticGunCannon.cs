﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
using System.Linq;
using System.Reflection;
#endif

[RequireComponent(typeof(CircleCollider2D))]
public class AutomaticGunCannon : MonoBehaviour
{
    public enum Directions
    {
        NORTH, SOUTH, EAST, WEST
    }

    public enum TargetMode
    {
        FIXED, TARGET, RANDOM
    }

    public enum Targeting
    {
        AFTER_SHOT, BEFORE_SHOT, AFTER_DESTROY_TARGET
    }

    public enum Priority
    {
        MIN_LIFE, MAX_LIFE, FROM_PERFOMED_DAMAGE
    }

    public enum ShotMode
    {
        NONE, ONE_SHOT, RAFAGUE, AUTOMATIC
    }

    public TargetMode targetMode;
    public Directions direction;
    public Targeting targeting;
    public Priority priority;
    public float rangue = 1f;
    public List<string> targets = new List<string>();
    public bool isWithelist = true;

    public ShotMode shotMode;
    public float shotCooldown = 0.1f;
    private float shotTime;
    public Entity shooter;

    private List<IDamageable> targetsInRangue = new List<IDamageable>();
    private IDamageable target;

    CircleCollider2D _collider;

    public Ammo ammo;

    private Bullet[] bulletPool;
    private int bulletIndex = 0;
    private int bulletMaxPool = 10;

    CircleCollider2D Area
    {
        get
        {
            if (_collider == null)
                _collider = GetComponent<CircleCollider2D>();

            return _collider;
        }
    }

    private void Start()
    {
        shotTime = Time.time + shotCooldown;

        bulletPool = new Bullet[bulletMaxPool];

        for (int i = 0; i < bulletPool.Length; i++)
        {
            bulletPool[i] = ammo.GetBullet();
        }
    }

    private void Update()
    {
        if (shotMode == ShotMode.NONE)
            return;

        if (Time.time >= shotTime)
        {
            shotTime = Time.time + shotCooldown;

            switch (targeting)
            {
                case Targeting.AFTER_SHOT:
                    Shot();
                    Retargeting();
                    break;
                case Targeting.BEFORE_SHOT:
                    Retargeting();
                    Shot();
                    break;
                case Targeting.AFTER_DESTROY_TARGET:
                    Debug.Log("Not supported yet!");
                    if (target == null)
                        Retargeting();
                    Shot();
                    break;
            }
        }
    }

    public void Shot()
    {
        switch (targetMode)
        {
            case TargetMode.FIXED:
                switch (direction)
                {
                    case Directions.NORTH:
                        Shoot(transform.position, 90);
                        break;
                    case Directions.SOUTH:
                        Shoot(transform.position, 270);
                        break;
                    case Directions.EAST:
                        Shoot(transform.position, 0);
                        break;
                    case Directions.WEST:
                        Shoot(transform.position, 180);
                        break;
                }
                break;
            case TargetMode.TARGET:
                if(target != null)
                {
                    Vector3 relativePos = target.Transform.position - transform.position;
                    float angle = Vector3.SignedAngle(transform.position, relativePos, transform.forward);
                    Shoot(transform.position, angle);
                }
                break;
            case TargetMode.RANDOM:
                Shoot(transform.position, Random.Range(0,360));
                break;
        }
    }

    private void Shoot(Vector3 pos, float angle)
    {
        bulletPool[bulletIndex].Shoot(pos, angle, shooter);

        bulletIndex++;

        if (bulletIndex >= bulletMaxPool)
            bulletIndex = 0;
    }

    public void Retargeting()
    {
        if (targetsInRangue.Count == 0)
        {
            target = null;
            return;
        }

        switch (priority)
        {
            case Priority.MIN_LIFE:
                IDamageable minlife = targetsInRangue[0];
                foreach (IDamageable target in targetsInRangue)
                {
                    if (target.CurrentLife < minlife.CurrentLife)
                        minlife = target;
                }
                target = minlife;
                break;
            case Priority.MAX_LIFE:
                IDamageable maxLife = targetsInRangue[0];
                foreach (IDamageable target in targetsInRangue)
                {
                    if (target.CurrentLife > maxLife.CurrentLife)
                        maxLife = target;
                }
                target = maxLife;
                break;
            case Priority.FROM_PERFOMED_DAMAGE:
                break;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var component = collision.GetComponent<IDamageable>();

        if (component != null)
        {
            if (isWithelist)
            {
                for (int i = 0; i < targets.Count; i++)
                {
                    if (targets[i] == component.GetType().Name)
                        if (!targetsInRangue.Contains(component))
                            targetsInRangue.Add(component);
                }
            }
            else
            {
                bool found = false;

                for (int i = 0; i < targets.Count; i++)
                {
                    if (targets[i] == component.GetType().Name)
                    {
                        found = true;
                        break;
                    }
                }

                if(!found)
                    if (!targetsInRangue.Contains(component))
                        targetsInRangue.Add(component);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        var component = collision.GetComponent<IDamageable>();

        if (component != null)
            targetsInRangue.Remove(component);
    }

    public void OnValidate()
    {
        Area.radius = rangue;
    }
}

#if UNITY_EDITOR

[CustomEditor(typeof(AutomaticGunCannon))]
public class AutomaticGunCannonEditor: Editor
{
    AutomaticGunCannon myTarget;

    List<System.Type> aviableTargets;

    private void OnEnable()
    {
        myTarget = (AutomaticGunCannon)target;
        aviableTargets = new List<System.Type>();

        foreach (System.Type mytype in Assembly.GetExecutingAssembly().GetTypes().Where(mytype => mytype.GetInterfaces().Contains(typeof(IDamageable))))
        {
            aviableTargets.Add(mytype);
        }

    }

    public override void OnInspectorGUI()
    {
        myTarget.shooter = (Entity)EditorGUILayout.ObjectField("Entity", myTarget.shooter, typeof(Entity), true);
        myTarget.ammo = (Ammo)EditorGUILayout.ObjectField("Ammo", myTarget.ammo, typeof(Ammo), false);

        EditorGUILayout.Space();

        myTarget.shotMode = (AutomaticGunCannon.ShotMode)EditorGUILayout.EnumPopup("Shot mode", myTarget.shotMode);
        myTarget.shotCooldown = EditorGUILayout.Slider("Shot cooldown", myTarget.shotCooldown, 0.1f, 5);

        EditorGUILayout.Space();

        myTarget.targetMode = (AutomaticGunCannon.TargetMode)EditorGUILayout.EnumPopup("Target mode", myTarget.targetMode);

        switch (myTarget.targetMode)
        {
            case AutomaticGunCannon.TargetMode.FIXED:
                PaintFixedMode();
                break;
            case AutomaticGunCannon.TargetMode.TARGET:
                PaintTargetMode();
                break;
            case AutomaticGunCannon.TargetMode.RANDOM:
                PaintRandomMode();
                break;
        }

        if (GUI.changed)
            myTarget.OnValidate();
    }

    private void PaintFixedMode()
    {
        myTarget.direction = (AutomaticGunCannon.Directions)EditorGUILayout.EnumPopup("Direction", myTarget.direction);
    }

    private int FindIndexInAviableTargets(string name)
    {
        for (int i = 0; i < aviableTargets.Count; i++)
        {
            if (aviableTargets[i].Name == name)
            {
                return i;
            }
        }

        return 0;
    }

    private string[] GetAviableTargetList()
    {
        var list = new string[aviableTargets.Count];

        for (int i = 0; i < aviableTargets.Count; i++)
        {
            list[i] = aviableTargets[i].Name;
        }

        return list;
    }

    private void PaintTargetMode()
    {
        myTarget.targeting = (AutomaticGunCannon.Targeting)EditorGUILayout.EnumPopup("Targeting", myTarget.targeting);
        myTarget.priority = (AutomaticGunCannon.Priority)EditorGUILayout.EnumPopup("Priority", myTarget.priority);
        myTarget.rangue = EditorGUILayout.Slider("Rangue", myTarget.rangue, 0.1f, 10);
        myTarget.isWithelist = EditorGUILayout.Toggle("Is whitelist", myTarget.isWithelist);

        for (int i = 0; i < myTarget.targets.Count; i++)
        {
            EditorGUILayout.BeginHorizontal();
            myTarget.targets[i] = aviableTargets[EditorGUILayout.Popup("Target:", FindIndexInAviableTargets(myTarget.targets[i]), GetAviableTargetList())].Name;

            if (GUILayout.Button("Remove"))
            {
                myTarget.targets.Remove(aviableTargets[i].Name);
                return;
            }
            EditorGUILayout.EndHorizontal();
        }

        if (GUILayout.Button("Add"))
        {
            for (int i = 0; i < aviableTargets.Count; i++)
            {
                if (!myTarget.targets.Contains(aviableTargets[i].Name))
                {
                    myTarget.targets.Add(aviableTargets[i].Name);
                    break;
                }
            }
        }
    }

    private void PaintRandomMode()
    {

    }
}

#endif

