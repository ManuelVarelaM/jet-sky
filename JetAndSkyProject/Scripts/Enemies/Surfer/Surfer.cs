﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(CircleCollider2D), typeof(Life))]
public class Surfer : Enemy, IDamageable {

    public float speed = 1f;
    private float newSpeed;

    Wave closestWave;
    private bool isWaveFound;

    public UnityEvent onAiming;
    public UnityEvent onAttack;

    private Life _life;

    public Life Life
    {
        get
        {
            if (_life == null)
                _life = GetComponent<Life>();

            return _life;
        }
    }

    public float MaxLife
    {
        get
        {
            return Life.GetMaxLife();
        }
    }

    public float CurrentLife
    {
        get
        {
            return Life.GetCurrentLife();
        }
    }

    public Transform Transform
    {
        get
        {
            return transform;
        }
    }

    void Start()
    {
        newSpeed = speed;
    }

    public void SetSpeed(float speed)
    {
        this.speed = speed;
        newSpeed = speed;
    }

    public void ChangueSpeed(float speed)
    {
        newSpeed = speed;
    }

    void Update ()
    {
        if (!isWaveFound)
        {
            LinearMovement();
        }
        else
        {
            FollowWave();
        }

        if (speed > newSpeed)
        {
            speed -= Time.deltaTime * 3;
        }
        else if(speed < newSpeed)
        {
            speed += Time.deltaTime * 3;
        }
    }

    public void Attack()
    {
        onAttack.Invoke();
    }

    public void Aiming()
    {
        onAiming.Invoke();
    }

    private void LinearMovement()
    {
        transform.position -= transform.up * Time.deltaTime * speed;
    }

    private void FollowWave()
    {
        transform.position = Vector2.Lerp(transform.position, closestWave.transform.position, Time.deltaTime * speed);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!isWaveFound)
        {
            closestWave = other.GetComponent<Wave>();

            isWaveFound = closestWave != null;
        }
    }

    public void GetDamage(float i)
    {
        Life.Damage(i);
    }
}
