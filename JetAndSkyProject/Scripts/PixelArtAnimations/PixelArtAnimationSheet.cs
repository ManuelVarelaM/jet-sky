﻿using UnityEngine;

[CreateAssetMenu(menuName = "Jet&Sky/Pixel Art Animations/Create")]
public class PixelArtAnimationSheet : ScriptableObject
{
    [SerializeField]
    private Sprite[] sprites;

    [SerializeField]
    private int columns, rows;

    public int Columns
    {
        get
        {
            return columns;
        }
    }

    public int Rows
    {
        get
        {
            return rows;
        }
    }

    public Sprite[] GetAnimationSheet()
    {
        return sprites;
    }

    public Sprite Get(int column, int row)
    {
        return sprites[column + (row * columns)];
    }

    public void SetSize(int column, int row)
    {
        columns = column;
        rows = row;

        sprites = new Sprite[column * row];
    }

}