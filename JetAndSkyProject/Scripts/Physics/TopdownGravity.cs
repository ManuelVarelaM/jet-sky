﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopdownGravity : MonoBehaviour {

    public float gravity;
    public bool isKinematic;
    private float velocityY;
    private bool isGrounded;

	void Start () {

        isGrounded = (transform.position.z >= 0);
	}

	void Update () {
        if (!isKinematic)
        {
            if (!isGrounded)
            {
                velocityY -= Time.deltaTime * gravity;

                Vector3 nextPos = transform.position;
                nextPos.z -= velocityY;
                transform.position = nextPos;

                if (transform.position.z >= 0)
                {
                    isGrounded = true;
                    transform.position = new Vector3(transform.position.x, transform.position.y, 0);
                    velocityY = 0;
                }
            }
        }
	}

    public bool IsGrounded()
    {
        return isGrounded;
    }

    public void AddForce(float delta)
    {
        velocityY += delta;
        isGrounded = false;
    }
}
