﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Goal : ScriptableObject {

    public abstract bool Reading();
}
