﻿using UnityEngine;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(menuName = "Jet&Sky/Level Controller/Goals/Wait")]
public class Goal_Wait : Goal
{
    public float delayInSeconds = 1;

    private float waitingTime;
    private bool init;

    public override bool Reading()
    {
        if(!init)
        {
            waitingTime = Time.time + delayInSeconds;
            init = true;
        }

        if (Time.time >= waitingTime)
        {
            init = false;
            return true;
        }

        return false;
    }
}

#if UNITY_EDITOR

[CustomEditor(typeof(Goal_Wait))]
public class GoalWaitEditor : Editor
{
    Goal_Wait myTarget; 

    private void OnEnable()
    {
        myTarget = (Goal_Wait)target;
    }

    public override void OnInspectorGUI()
    {
        myTarget.delayInSeconds = EditorGUILayout.Slider("Seconds", myTarget.delayInSeconds, 0, 60);

        if (GUI.changed)
            EditorUtility.SetDirty(myTarget);
    }
}

#endif