﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditorInternal;
#endif

[CreateAssetMenu(menuName = "Jet&Sky/Level Controller/Controller")]
public class LevelController : ScriptableObject
{
    public bool deselectOnRead;
    public FloatValue speed;

    [System.Serializable]
    public class LevelGoal
    {
        public string id;
        public bool isOver;
        //public enum Type
        //{
        //    SPAWN, CONDITION, RING, EFFECT, DIALOGUE, ENVIROMENT
        //}

        //public Type type;

        public Goal goal;

        public void Read()
        {
            if (!isOver)
            {
                isOver = goal.Reading();
            }
        }
    }

    public List<LevelGoal> waves = new List<LevelGoal>();
}

#if UNITY_EDITOR
[CustomEditor(typeof(LevelController))]
public class LevelDataEditor : Editor
{
    private SerializedProperty m_speed;
    private SerializedProperty m_deselectOnRead;
    private ReorderableList list;

    private void OnEnable()
    {
        m_speed = serializedObject.FindProperty("speed");
        m_deselectOnRead = serializedObject.FindProperty("deselectOnRead");

        list = new ReorderableList(serializedObject,
                serializedObject.FindProperty("waves"),
                true, true, true, true);

        list.drawElementCallback =
        (Rect rect, int index, bool isActive, bool isFocused) => {
            var element = list.serializedProperty.GetArrayElementAtIndex(index);
            rect.y += 2;

            EditorGUI.PropertyField(
                new Rect(rect.x, rect.y, 20, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("isOver"), GUIContent.none);

            EditorGUI.PropertyField(
                new Rect(rect.x + 30, rect.y, 100, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("id"), GUIContent.none);

            EditorGUI.PropertyField(
                new Rect(rect.x + 130, rect.y, 100, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("goal"), GUIContent.none);
        };
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        EditorGUILayout.PropertyField(m_deselectOnRead, new GUIContent("Deselect On Load"), GUILayout.Height(20));
        EditorGUILayout.Space();
        EditorGUILayout.PropertyField(m_speed, new GUIContent("Level Speed"), GUILayout.Height(20));
        list.DoLayoutList();
        serializedObject.ApplyModifiedProperties();

        if (GUILayout.Button("Dismark All"))
        {
            foreach (LevelController.LevelGoal lg in ((LevelController)target).waves)
            {
                lg.isOver = false;
            }
        }
    }
}
#endif
