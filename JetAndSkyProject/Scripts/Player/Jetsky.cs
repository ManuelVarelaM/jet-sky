﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Life), typeof(Rigidbody), typeof(Jump))]
public class Jetsky : Entity, IDamageable {

    [SerializeField]
    private ControllerTemplate template;

    [SerializeField]
    private readonly float turnSpeed = 3;

    [SerializeField]
    private float jumpForce = 100;

    [SerializeField] LayerMask whatIsGround;

    public GameObject Jet;
    public GameObject Sky;

    private Life _life;

    private Rigidbody _rg;
    private Jump jump;
    public GameObject water_splash;

    private bool canDash = true;
    private bool onLand;
    private bool dashing;

    public Vector2 direction;

    public float timeSpeed = 1f;
    public float localTime;

    public float speedX;
    public float speedY;
    public float maxSpeedX;
    public float maxSpeedY;

    public float accelX;
    public float accelY;
    public float decelX;
    public float decelY;

    public float dashCooldown;
    private float dashTimer;
    
    private void Start()
    {
        jump = GetComponent<Jump>();
    }

    private void Update()
    {
        Debug.Log("wena");

        direction = new Vector3(template.leftController.Horizontal(), template.leftController.Vertical()).normalized;
        localTime = timeSpeed * Time.fixedDeltaTime;

        // Saltando
        if (!jump.onFloor)
        {
            canDash = false;
            water_splash.SetActive(false);

            maxSpeedX = 30;
            maxSpeedY = 30;

            accelX = 6;
            accelY = 6;
            decelX = 1;
            decelY = 1;
        }

        // Sobre tierra
        if (onLand)
        {
            canDash = false;
            water_splash.SetActive(false);

            maxSpeedX = 20;
            maxSpeedY = 20;

            accelX = 4;
            accelY = 4;
            decelX = 1;
            decelY = 1;
        }

        // Sobre agua
        else
        {
            // Mostrar splash de agua
            water_splash.SetActive(true);

            // Puede hacer dash?
            if (dashTimer > 0)
            {
                dashTimer -= localTime;
                canDash = false;
            }

            else
            {
                dashTimer = 0;
                canDash = true;
            }

            // Esta haciendo dash
            if (dashing)
            {
                maxSpeedX = 2.2f;
                maxSpeedY = 2;

                accelX = 20;
                accelY = 20;
                decelX = 0;
                decelY = 0;

                dashTimer += localTime;

                if (dashTimer >= dashCooldown)
                    dashing = false;
            }

            // No esta haciendo dash
            else
            {
                maxSpeedX = 1.2f;
                maxSpeedY = 0.8f;

                accelX = 6;
                accelY = 6;
                decelX = 2;
                decelY = 2;
            }

            // Activar dash
            if (template.leftController.Action() && !dashing)
            {
                dashing = true;
                dashTimer = 0;
            }
        }
        
        // Aceleracion y desaceleracion
        float newSpeedX = speedX + accelX * direction.x * localTime;
        float newSpeedY = speedY + accelY * direction.y * localTime;

        // Aceleracion X
        if (direction.x != 0)
        {
            if (newSpeedX > maxSpeedX)
                newSpeedX = maxSpeedX;

            else if (newSpeedX < -maxSpeedX)
                newSpeedX = -maxSpeedX;
        }

        // Desaceleracion X
        else
        {
            if (speedX > 0)
                newSpeedX -= decelX * localTime;

            else if (speedX < 0)
                newSpeedX += decelX * localTime;
        }

        // Aceleracion Y
        if (direction.y != 0)
        {
            if (newSpeedY > maxSpeedY)
                newSpeedY = maxSpeedY;

            else if (newSpeedY < -maxSpeedY)
                newSpeedY = -maxSpeedY;
        }

        // Desaceleracion Y
        else
        {
            if (speedY > 0)
                newSpeedY -= decelY * localTime;

            else if (speedY < 0)
                newSpeedY += decelY * localTime;
        }

        speedX = newSpeedX;
        speedY = newSpeedY;

        // Dash
        if (template.leftController.Action() && canDash)
        {
            Dash();
        }

        // Fuerza de salto
        if (dashing)
            jump.force = 12;

        else
            jump.force = 5;
    }

    private void FixedUpdate()
    {
        Vector3 speed = new Vector3(Mathf.Round(speedX * 1000), Mathf.Round(speedY * 1000), 0);
        
        // 'rotate' indica el angulo hacia el que debe girar la moto de agua, segun su direccion x
        float rotate;

        // Hacia la izquierda
        if (direction.x < 0)
            rotate = 15;

        // Hacia la derecha
        else if (direction.x > 0)
            rotate = -15;

        else
            rotate = 0;

        transform.position = transform.position + new Vector3(speedX / 20, speedY / 20, 0);
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, 0, rotate), 0.2f);

        water_splash.transform.position = transform.position;
        water_splash.transform.rotation = Transform.rotation;

        RB.position = transform.position;
    }

    private void Dash()
    {
        dashing = true;
        dashTimer = 0;
        canDash = false;
    }

    private Rigidbody RB
    {
        get
        {
            if (_rg == null)
                _rg = GetComponent<Rigidbody>();

            return _rg;
        }
    }

    public Life Life
    {
        get
        {
            if (_life == null)
                _life = GetComponent<Life>();

            return _life;
        }
    }

    public float MaxLife
    {
        get
        {
            return Life.GetMaxLife();
        }
    }

    public float CurrentLife
    {
        get
        {
            return Life.GetCurrentLife();
        }
    }

    public void GetDamage(float i)
    {
        Life.Damage(i);
    }

    public Transform Transform
    {
        get
        {
            return transform;
        }
    }
}
