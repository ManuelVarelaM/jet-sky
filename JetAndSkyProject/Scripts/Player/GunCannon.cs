﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunCannon : MonoBehaviour {

    [SerializeField]
    private ControllerTemplate template;

    [SerializeField]
    private GameEvent onShot;

    [SerializeField] AudioSource shootSource;

    public Entity shooter;
    public Ammo ammo;

    private Bullet[] bulletPool;
    private int bulletIndex = 0;
    private int bulletMaxPool = 20;

    private void Start()
    {
        bulletPool = new Bullet[bulletMaxPool];

        for (int i = 0; i < bulletPool.Length; i++)
        {
            bulletPool[i] = ammo.GetBullet();
        }
    }

    public void Shoot(Vector3 pos, float angle)
    {
        bulletPool[bulletIndex].Shoot(pos, angle, shooter);

        bulletIndex++;

        if (bulletIndex >= bulletMaxPool)
            bulletIndex = 0;

        onShot.Raise();

        shootSource.Play();
    }
}
