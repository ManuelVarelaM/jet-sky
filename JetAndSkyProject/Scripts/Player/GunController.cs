﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class GunController : MonoBehaviour {

    [SerializeField]
    private ControllerTemplate template;

    [SerializeField]
    private GunCannon gunCannon;

    [SerializeField]
    private PixelArtAnimationSheet aimginSheet;

    [SerializeField]
    private Sprite[] _aim_sprites;

    [SerializeField]
    private GameEvent onHeatUpdate;

    [SerializeField]
    private GameEvent onHeatWeapon;

    [SerializeField]
    public float heatCoolDown = 1;
    private float heatCDTime;

    private SpriteRenderer _aim;
    private SpriteRenderer _pilot;

    private uint overheating_index;

    private int shootState = 2;

    public float shootHeat = 1;
    private float heat = 1;
    private bool overHeat;
    private float coolDownTime = 0.1f;

    public float rotationSpeed;
    private float angle;
    private float toAngle;

    private Sprite Aim_Sprite
    {
        get
        {
            return _aim_sprites[overheating_index];
        }
    }

    private Sprite Pilot
    {
        get
        {
            return _pilot.sprite;
        }
    }

    private SpriteRenderer Aim
    {
        get
        {
            if (_aim == null)
            {
                _aim = new GameObject("Aim").AddComponent<SpriteRenderer>();
                _aim.sprite = Aim_Sprite;
            }

            return _aim;
        }
    }

    private void Start()
    {
        _pilot = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        if (template.rightController.GetControllerType() == ControllerSide.ControllerType.MOUSE)
        {
            ScreenAim();
        }
        else
        {
            CircularAim();
        }

        if (template.rightController.Action() && heat <= 1 && !overHeat)
        {
            coolDownTime -= Time.deltaTime;

            shootState = 1;

            if (coolDownTime < 0)
            {
                shootState = 0;
                coolDownTime = 0.1f;
                heat += shootHeat * Time.deltaTime;
                heatCDTime = Time.time + heatCoolDown;

                if (heat >= 1)
                {
                    heatCDTime = Time.time + (heatCoolDown * 3);
                    overHeat = true;
                    onHeatWeapon.Raise();
                }

                gunCannon.Shoot(transform.position, angle);
            }
        }
        else
        {
            coolDownTime = 0.1f;
            shootState = 2;

            if (Time.time > heatCDTime)
            {
                heat -= Time.deltaTime * Mathf.Pow(shootHeat, shootHeat);

                if (heat <= 0)
                {
                    overHeat = false;
                }
            }
        }

        heat = Mathf.Clamp01(heat);

        onHeatUpdate.Raise();
    }

    private void ScreenAim()
    {
        Vector3 aimPos = GetWorldPositionOnPlane(Input.mousePosition, 0);
        aimPos.z = -1;
        Aim.transform.position = aimPos;
        Vector3 dir = aimPos - transform.position;
        toAngle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;

        float deltaAngle = Mathf.DeltaAngle(angle, toAngle);

        if (deltaAngle > rotationSpeed / 2)
        {
            angle += rotationSpeed;
        }

        else if (deltaAngle < - rotationSpeed / 2)
        {
            angle -= rotationSpeed;
        }

        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        _pilot.sprite = Pilot;
        UpdateAnimationState(angle);
    }

    public Vector3 GetWorldPositionOnPlane(Vector3 screenPosition, float z)
    {
        Ray ray = Camera.main.ScreenPointToRay(screenPosition);
        Plane xy = new Plane(Vector3.forward, new Vector3(0, 0, z));
        float distance;
        xy.Raycast(ray, out distance);
        return ray.GetPoint(distance);
    }

    private void CircularAim()
    {
        //Vector3 axispos;
        //axispos.normalized;
    }

    private void UpdateAnimationState(float angle)
    {
        _pilot.sprite = aimginSheet.Get(shootState, 1);
    }
}
